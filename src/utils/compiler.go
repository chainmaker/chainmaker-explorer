package utils

import (
	"chainmaker_web/src/config"
	"chainmaker_web/src/entity"
	"chainmaker_web/src/models/compiler"
	"encoding/json"
	"fmt"
	"mime/multipart"
	"strconv"
)

// GetCrossGatewayInfo
//
//	@Description: 根据gatewayId获取子链列表
//	@param gatewayId 网关id
//	@return *relayCrossChain.GatewayInfoData 子链列表
//	@return error
func HttpGetCompilerVersions() (compiler.VersionsData, error) {
	compilerUrl := config.GlobalConfig.WebConf.ContractCompileUrl
	url := compilerUrl + CmbGetCompilerVersions
	body, err := SendHttpGetRequest(url, nil)
	result := compiler.VersionsData{}
	if err != nil {
		log.Errorf("【http】 HttpGetCompilerVersions err:%v", err)
		return result, err
	}
	var respJson compiler.CompilerVersionsResponse
	err = json.Unmarshal(body, &respJson)
	if err != nil {
		log.Errorf("【http】 HttpGetCompilerVersions err:%v", err)
		return result, err
	}

	result = respJson.Data
	return result, nil
}

// GetCrossGatewayInfo
//
//	@Description: 根据gatewayId获取子链列表
//	@param gatewayId 网关id
//	@return *relayCrossChain.GatewayInfoData 子链列表
//	@return error
func HttpGetEvmVersions() (compiler.VersionsData, error) {
	compilerUrl := config.GlobalConfig.WebConf.ContractCompileUrl
	url := compilerUrl + CmbGetEvmVersions
	body, err := SendHttpGetRequest(url, nil)
	result := compiler.VersionsData{}
	if err != nil {
		log.Errorf("【http】 HttpGetCompilerVersions err:%v", err)
		return result, err
	}
	var respJson compiler.CompilerVersionsResponse
	err = json.Unmarshal(body, &respJson)
	if err != nil {
		log.Errorf("【http】 HttpGetCompilerVersions err:%v", err)
		return result, err
	}

	result = respJson.Data
	return result, nil
}

// SendPostContractCompile 发送合约编译请求，返回编译ID
func SendPostContractCompile(params *entity.VerifyContractParams) (string, error) {
	// 将结构体转换为map
	paramMap := make(map[string]string)
	paramMap["chainId"] = params.ChainId
	paramMap["contractAddr"] = params.ContractAddr
	paramMap["contractVersion"] = params.ContractVersion
	paramMap["compilerPath"] = params.CompilerPath
	paramMap["compilerVersion"] = params.CompilerVersion
	paramMap["openLicenseType"] = params.OpenLicenseType
	paramMap["optimization"] = strconv.FormatBool(params.Optimization)
	paramMap["runs"] = strconv.Itoa(params.Runs)
	paramMap["evmVersion"] = params.EvmVersion

	// 将文件添加到一个切片中
	files := map[string]*multipart.FileHeader{
		"contractSourceFile": params.ContractSourceFile,
	}

	// 发送请求
	compilerUrl := config.GlobalConfig.WebConf.ContractCompileUrl
	url := compilerUrl + CmbContractCompile
	body, err := SendHttpPostRequestNew(url, paramMap, files)
	//log.Infof("[http] SendPostContractCompile body:%v", string(body))
	if err != nil {
		log.Errorf("Failed to send HTTP POST request: %v", err)
		return "", err
	}

	var respJson compiler.ContractCompileResponse
	err = json.Unmarshal(body, &respJson)
	if err != nil {
		log.Errorf("【http】 SendPostContractCompile err:%v", err)
		return "", err
	}

	if respJson.Data != nil {
		return respJson.Data.CompileID, nil
	}
	return "", fmt.Errorf("%s", respJson.Message)
}

// GetCrossGatewayInfo
//
//	@Description: 根据gatewayId获取子链列表
//	@param gatewayId 网关id
//	@return *relayCrossChain.GatewayInfoData 子链列表
//	@return error
func HttpGetContractCompileResult(compileID string) (*compiler.CompilerRersultResponse, error) {
	compilerUrl := config.GlobalConfig.WebConf.ContractCompileUrl
	url := fmt.Sprintf("%s%s&compileID=%s", compilerUrl, CmbGetGetContractCompileResult, compileID)
	body, err := SendHttpGetRequest(url, nil)
	//log.Infof("[http] SendPostContractCompile body:%v", string(body))

	if err != nil {
		log.Errorf("【http】 HttpGetContractCompileResult err:%v", err)
		return nil, err
	}
	var respJson *compiler.CompilerRersultResponse
	err = json.Unmarshal(body, &respJson)
	if err != nil {
		log.Errorf("【http】 HttpGetContractCompileResult err:%v", err)
		return nil, err
	}
	return respJson, nil
}
