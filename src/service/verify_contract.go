package service

import (
	"archive/zip"
	"bytes"
	"chainmaker_web/src/code"
	"chainmaker_web/src/config"
	"chainmaker_web/src/db"
	"chainmaker_web/src/db/dbhandle"
	"chainmaker_web/src/entity"
	"chainmaker_web/src/models/compiler"
	"chainmaker_web/src/sync/common"
	"chainmaker_web/src/utils"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

const (
	//VerifyStatusSuccess 合约编译中
	VerifyStatusDefault = 0
	//VerifyStatusSuccess 验证成功
	VerifyStatusSuccess = 1
	//VerifyStatusFailed 验证失败
	VerifyStatusFailed = 2
)

// VerifyContractSourceCodeHandler handler
type VerifyContractSourceCodeHandler struct{}

// Handle deal
func (handler *VerifyContractSourceCodeHandler) Handle(ctx *gin.Context) {
	params := entity.BindVerifyContractCodeHandler(ctx)
	if params == nil || !params.IsLegal() {
		newError := entity.NewError(entity.ErrorParamWrong, "param is wrong")
		ConvergeFailureResponse(ctx, newError)
		return
	}

	chainId := params.ChainId
	contractAddr := params.ContractAddr
	//数据库获取链上合约源码
	upgradeContract, err := dbhandle.GetUpgradeContractInfo(params.ChainId, contractAddr, params.ContractVersion)
	if err != nil {
		log.Errorf("GetUpgradeContractInfo err : %s", err.Error())
		ConvergeHandleFailureResponse(ctx, err)
		return
	}

	//仅支持EVM合约验证
	if upgradeContract.ContractRuntimeType != common.RuntimeTypeEVM {
		newError := entity.NewError(entity.ErrorHandleFailure, "仅支持EVM合约验证")
		ConvergeHandleFailureResponse(ctx, newError)
		return
	}

	//已经验证过了
	if upgradeContract.VerifyStatus == VerifyStatusSuccess {
		view := &entity.VerifyContractView{
			VerifyStatus: VerifyStatusSuccess,
		}
		ConvergeDataResponse(ctx, view, nil)
		return
	}

	//获取合约编译结果
	compilerResult, err := GetContractCompileResult(params)
	if err != nil {
		log.Errorf("GetContractCompileResult err : %s", err.Error())
		ConvergeHandleFailureResponse(ctx, err)
		return
	}

	//合约验证
	verifyStatus := VerifyStatusFailed
	contractByteCode := hex.EncodeToString(upgradeContract.ContractByteCode)
	if compilerResult.Bytecode == contractByteCode {
		verifyStatus = VerifyStatusSuccess
	}

	//保存合约验证结果
	contractName := upgradeContract.ContractName
	verifyResult, err := SaveContractVerifyResult(params, verifyStatus, compilerResult, contractName)
	if err != nil {
		log.Errorf("SaveContractVerifyResult err : %s", err.Error())
		ConvergeHandleFailureResponse(ctx, err)
		return
	}

	//更新合约验证结果
	err = dbhandle.UpdateUpgradeContractVerifyStatus(chainId, contractAddr, params.ContractVersion, verifyStatus)
	if err != nil {
		log.Errorf("UpdateUpgradeContractVerifyStatus err : %s", err.Error())
		ConvergeHandleFailureResponse(ctx, err)
		return
	}

	//保存合约源码
	if verifyStatus == VerifyStatusSuccess {
		err = SaveAndUnzipFileContractSource(chainId, params.ContractSourceFile, verifyResult)
		if err != nil {
			log.Errorf("SaveAndUnzipFileContractSource err : %s", err.Error())
			ConvergeHandleFailureResponse(ctx, err)
			return
		}
	}

	var message string
	if compilerResult.Bytecode == "" {
		if compilerResult.Message != "" {
			message = compilerResult.Message
		} else {
			message = code.ErrorContractCompilerFailed
		}
	} else if compilerResult.Bytecode != contractByteCode {
		message = code.ErrorContractCompilerDiff
	}

	view := &entity.VerifyContractView{
		VerifyStatus: verifyStatus,
		MetaData:     compilerResult.MetaData,
		Message:      message,
	}
	if verifyStatus == VerifyStatusFailed {
		view.CompilerByteCode = compilerResult.Bytecode
		view.ContractByteCode = contractByteCode
	}

	ConvergeDataResponse(ctx, view, nil)
}

func SaveContractVerifyResult(params *entity.VerifyContractParams, verifyStatus int,
	compilerResult *compiler.CompilerResult, contractName string) (*db.EVMContractVerifyResult, error) {
	if compilerResult == nil {
		return nil, nil
	}
	verifyResult, err := dbhandle.GetContractVerifyResult(params.ChainId, params.ContractAddr, params.ContractVersion)
	if err != nil {
		return nil, err
	}

	// 将 ABI 字段转换为 JSON 字符串
	var abiStr string
	if compilerResult.ABI != "" {
		abiJSON, _ := json.Marshal(compilerResult.ABI)
		abiStr = string(abiJSON)
	}

	//新增
	if verifyResult == nil {
		newUUID := uuid.New().String()
		// 创建 ContractSourceFile 实例
		verifyInfo := &db.EVMContractVerifyResult{
			VerifyId:        newUUID,
			VerifyStatus:    verifyStatus,
			ContractAddr:    params.ContractAddr,
			ContractName:    contractName,
			ContractVersion: params.ContractVersion,
			CompilerPath:    params.CompilerPath,
			ByteCode:        []byte(compilerResult.Bytecode),
			ABI:             abiStr,
			MetaData:        compilerResult.MetaData,
			CompilerVersion: params.CompilerVersion,
			OpenLicenseType: params.OpenLicenseType,
			EvmVersion:      params.EvmVersion,
			Optimization:    params.Optimization,
			RunNum:          params.Runs,
		}

		// 将 ContractVerify 实例保存到数据库
		err = dbhandle.InsertContractVerifyResult(params.ChainId, verifyInfo)
		return verifyInfo, err
	}

	if verifyResult.VerifyStatus == VerifyStatusSuccess {
		return verifyResult, nil

	}

	//更新
	verifyResult.VerifyStatus = verifyStatus
	verifyResult.CompilerPath = params.CompilerPath
	verifyResult.ByteCode = []byte(compilerResult.Bytecode)
	verifyResult.ABI = abiStr
	verifyResult.MetaData = compilerResult.MetaData
	verifyResult.CompilerVersion = params.CompilerVersion
	verifyResult.OpenLicenseType = params.OpenLicenseType
	verifyResult.EvmVersion = params.EvmVersion
	verifyResult.Optimization = params.Optimization
	verifyResult.RunNum = params.Runs
	// 将 ContractVerify 实例保存到数据库
	err = dbhandle.UpdateContractVerifyResult(params.ChainId, verifyResult)
	return verifyResult, err
}

func SaveAndUnzipFileContractSource(chainId string, fileHeader *multipart.FileHeader,
	verifyResult *db.EVMContractVerifyResult) error {
	// 打开上传的文件
	file, err := fileHeader.Open()
	if err != nil {
		return err
	}
	defer file.Close()

	// 读取文件内容
	fileContent, err := io.ReadAll(file)
	if err != nil {
		return err
	}

	// 创建一个新的zip reader
	zipReader, err := zip.NewReader(bytes.NewReader(fileContent), int64(len(fileContent)))
	if err != nil {
		return err
	}

	sourceFileList := make([]*db.ContractSourceFile, 0)
	// 遍历压缩包中的每个文件
	for _, zipFile := range zipReader.File {
		// 过滤掉工具增加的文件
		if isIgnoredFile(zipFile.Name) {
			log.Infof("Skipping ignored file: %s\n", zipFile.Name)
			continue
		}

		// 打开压缩包中的文件
		zipFileReader, errOpen := zipFile.Open()
		if errOpen != nil {
			zipFileReader.Close()
			return errOpen
		}

		// 读取压缩包中的文件内容
		zipFileContent, errRead := io.ReadAll(zipFileReader)
		if errRead != nil {
			zipFileReader.Close()
			return errRead
		}

		// 检查文件内容是否为空
		if len(zipFileContent) == 0 {
			log.Infof("Skipping empty file: %s\n", zipFile.Name)
			// 关闭压缩包中的文件
			zipFileReader.Close()
			continue
		}

		newUUID := uuid.New().String()
		// 创建 ContractSourceFile 实例
		sourceFile := &db.ContractSourceFile{
			ID:              newUUID,
			VerifyId:        verifyResult.VerifyId,
			ContractAddr:    verifyResult.ContractAddr,
			ContractVersion: verifyResult.ContractVersion,
			SourcePath:      zipFile.Name, // 使用压缩包内部的文件路径
			SourceCode:      zipFileContent,
		}

		sourceFileList = append(sourceFileList, sourceFile)

		// 关闭压缩包中的文件
		zipFileReader.Close()
	}

	// 将 ContractSourceFile 实例保存到数据库
	err = dbhandle.InsertContractSource(chainId, sourceFileList)
	return err
}

// isIgnoredFile 检查文件名是否是被忽略的文件
func isIgnoredFile(fileName string) bool {
	// 定义被忽略的文件名列表
	ignoredFiles := []string{
		".DS_Store",
		"__MACOSX",
		"Thumbs.db",
		"desktop.ini",
	}

	// 遍历被忽略的文件名列表
	for _, ignored := range ignoredFiles {
		// 如果文件名包含被忽略的文件名，则返回true
		if strings.Contains(fileName, ignored) {
			return true
		}
	}
	// 如果文件名不包含被忽略的文件名，则返回false
	return false
}

// GetContractCompileResult 获取合约编译结果
// @params params 	合约验证参数
// @return *compiler.CompilerResult 	编译结果
// @return error 					错误信息
func GetContractCompileResult(params *entity.VerifyContractParams) (*compiler.CompilerResult, error) {
	result := &compiler.CompilerResult{}
	//触发合约源码编译，获取编译ID
	compilerId, err := utils.SendPostContractCompile(params)
	if err != nil {
		return result, err
	}

	var compilerResult *compiler.CompilerRersultResponse
	//根据编译ID获取编译结果，0：未验证，1:验证成功，2:验证失败
	//每秒轮询获取编译结果，直到状态不等于0结束循环
	//编译时间超过timeout后自动结束查询
	timeout := config.ContractCompilationTimeout
	startTime := time.Now()
	for {
		compilerResult, err = utils.HttpGetContractCompileResult(compilerId)
		if err != nil {
			return result, err
		}

		// 如果编译结果为 nil，继续轮询
		if compilerResult == nil {
			time.Sleep(1 * time.Second)
			continue
		}

		if compilerResult.Code != http.StatusOK {
			return result, fmt.Errorf("%s", compilerResult.Message)
		}

		// 检查编译状态，是否编译结束
		if compilerResult.Data != nil && compilerResult.Data.Status != VerifyStatusDefault {
			break
		}

		// 检查是否超时
		if time.Since(startTime) > timeout {
			result.Message = "合约编译超时"
			return result, nil
		}

		// 每次轮询后等待1秒
		time.Sleep(1 * time.Second)
	}

	return compilerResult.Data, nil
}

// GetContractVersionsHandler handler
type GetContractVersionsHandler struct{}

// Handle deal
func (handler *GetContractVersionsHandler) Handle(ctx *gin.Context) {
	params := entity.BindContractVersionsHandler(ctx)
	if params == nil || !params.IsLegal() {
		newError := entity.NewError(entity.ErrorParamWrong, "param is wrong")
		ConvergeFailureResponse(ctx, newError)
		return
	}

	//用户列表
	contractVersions, err := dbhandle.GetContractVersions(params.ChainId, params.ContractAddr)
	if err != nil {
		log.Errorf("GetContractVersions err : %s", err.Error())
		ConvergeHandleFailureResponse(ctx, err)
		return
	}

	//组装response数据
	view := &entity.VersionView{
		Versions: contractVersions,
	}
	ConvergeDataResponse(ctx, view, nil)
}

// GetCompilerVersionsHandler handler
type GetCompilerVersionsHandler struct{}

// Handle deal
func (handler *GetCompilerVersionsHandler) Handle(ctx *gin.Context) {
	//获取编译器版本列表
	versionResult, err := utils.HttpGetCompilerVersions()
	if err != nil {
		log.Errorf("HttpGetCompilerVersions err : %s", err.Error())
		ConvergeHandleFailureResponse(ctx, err)
		return
	}

	//组装response数据
	view := &entity.VersionView{
		Versions: versionResult.Versions,
	}
	ConvergeDataResponse(ctx, view, nil)
}

// GetEvmVersionsHandler handler
type GetEvmVersionsHandler struct{}

// Handle deal
func (handler *GetEvmVersionsHandler) Handle(ctx *gin.Context) {
	//获取编译器版本列表
	versionResult, err := utils.HttpGetEvmVersions()
	if err != nil {
		log.Errorf("HttpGetEvmVersions err : %s", err.Error())
		ConvergeHandleFailureResponse(ctx, err)
		return
	}

	//组装response数据
	view := &entity.VersionView{
		Versions: versionResult.Versions,
	}
	ConvergeDataResponse(ctx, view, nil)
}

// GetOpenLicenseTypesHandler handler
type GetOpenLicenseTypesHandler struct{}

// Handle deal
func (handler *GetOpenLicenseTypesHandler) Handle(ctx *gin.Context) {
	//组装response数据
	view := &entity.OpenLicenseTypeView{
		LicenseTypes: config.OpenLicenseTypes,
	}
	ConvergeDataResponse(ctx, view, nil)
}

// GetContractCodeHandler handler
type GetContractCodeHandler struct {
}

// Handle deal
func (getContractCodeHandler *GetContractCodeHandler) Handle(ctx *gin.Context) {
	params := entity.BindGetContractCodeHandler(ctx)
	if params == nil || !params.IsLegal() {
		newError := entity.NewError(entity.ErrorParamWrong, "param is wrong")
		ConvergeFailureResponse(ctx, newError)
		return
	}

	//获取合约验证结果
	verifyResult, err := dbhandle.GetContractVerifyResult(params.ChainId, params.ContractAddr, params.ContractVersion)
	if err != nil {
		log.Errorf("GetContractVerifyResult err : %s", err.Error())
		ConvergeHandleFailureResponse(ctx, err)
		return
	}

	if verifyResult == nil {
		ConvergeHandleFailureResponse(ctx, fmt.Errorf("合约还未验证"))
		return
	}

	//获取合约源码
	sourceFiles, err := dbhandle.GetContractSourceFile(params.ChainId, verifyResult.VerifyId)
	if err != nil {
		log.Errorf("GetContractSourceFile err : %s", err.Error())
		ConvergeHandleFailureResponse(ctx, err)
		return
	}

	//组装源码文件
	sourceCodes := make([]*entity.SourceCode, 0, len(sourceFiles))
	for _, file := range sourceFiles {
		sourceCodes = append(sourceCodes, &entity.SourceCode{
			SourcePath: file.SourcePath,
			SourceCode: string(file.SourceCode),
		})
	}

	contractCodeView := &entity.ContractCodeView{
		VerifyStatus:    verifyResult.VerifyStatus,
		ContractAbi:     verifyResult.ABI,
		SourceCodes:     sourceCodes,
		MetaData:        verifyResult.MetaData,
		ContractName:    verifyResult.ContractName,
		ContractAddr:    verifyResult.ContractAddr,
		ContractVersion: verifyResult.ContractVersion,
		CompilerVersion: verifyResult.CompilerVersion,
		EvmVersion:      verifyResult.EvmVersion,
		Optimization:    verifyResult.Optimization,
		Runs:            verifyResult.RunNum,
		OpenLicenseType: verifyResult.OpenLicenseType,
	}

	ConvergeDataResponse(ctx, contractCodeView, nil)
}
