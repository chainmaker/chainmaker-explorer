// nolint
package db

import (
	"chainmaker_web/src/config"
	"context"
	"encoding/json"
	"fmt"
	"net"
	"os"
	"regexp"
	"sync"
	"time"

	"github.com/redis/go-redis/v9"
	"github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/wait"
)

var nodesList = []*config.NodeInfo{
	{
		Addr:        "9.135.180.61:12301",
		OrgCA:       "-----BEGIN CERTIFICATE-----\nMIICnjCCAkSgAwIBAgIDBM78MAoGCCqGSM49BAMCMIGKMQswCQYDVQQGEwJDTjEQ\nMA4GA1UECBMHQmVpamluZzEQMA4GA1UEBxMHQmVpamluZzEfMB0GA1UEChMWd3gt\nb3JnMS5jaGFpbm1ha2VyLm9yZzESMBAGA1UECxMJcm9vdC1jZXJ0MSIwIAYDVQQD\nExljYS53eC1vcmcxLmNoYWlubWFrZXIub3JnMB4XDTIzMTIwMTA4NDMxNFoXDTMz\nMTEyODA4NDMxNFowgYoxCzAJBgNVBAYTAkNOMRAwDgYDVQQIEwdCZWlqaW5nMRAw\nDgYDVQQHEwdCZWlqaW5nMR8wHQYDVQQKExZ3eC1vcmcxLmNoYWlubWFrZXIub3Jn\nMRIwEAYDVQQLEwlyb290LWNlcnQxIjAgBgNVBAMTGWNhLnd4LW9yZzEuY2hhaW5t\nYWtlci5vcmcwWTATBgcqhkjOPQIBBggqhkjOPQMBBwNCAARIrtmDyFGTcWqIJyM8\nweHuk+9GqTkllI9E59P4h3Ms/jP8xBaa815Zkh1y5WPqFxqyN5rfrRhRMp8LqoeU\nuF+To4GWMIGTMA4GA1UdDwEB/wQEAwIBBjAPBgNVHRMBAf8EBTADAQH/MCkGA1Ud\nDgQiBCAPRq+/1wQPj8AkeVIyl8D6i0dgqvxy5euC+DF5WVuUNzBFBgNVHREEPjA8\ngg5jaGFpbm1ha2VyLm9yZ4IJbG9jYWxob3N0ghljYS53eC1vcmcxLmNoYWlubWFr\nZXIub3JnhwR/AAABMAoGCCqGSM49BAMCA0gAMEUCIQCSFT8YV2rsga4TyT/qs0Qp\nAv0aRTURq7XqEmnuX3fDGQIgC93pXvi6GY0T6beC80HR3ib/TmTQ8YvVsIt1p/Tk\nc6E=\n-----END CERTIFICATE-----\n",
		TLSHostName: "chainmaker.org",
	},
}
var ChainListConfigTest = &config.ChainInfo{
	ChainId:   "chain1",
	AuthType:  "permissionedwithcert",
	OrgId:     "wx-org1.chainmaker.org",
	HashType:  "SHA256",
	NodesList: nodesList,
	UserInfo: &config.UserInfo{
		UserSignKey: "-----BEGIN EC PRIVATE KEY-----\nMHcCAQEEILN+eElgD7gwq5t/Z/ZQ4JifW/8RC/YaVW2unaMko2AXoAoGCCqGSM49\nAwEHoUQDQgAEhw6eLytgitYparmL/ALv7k7GnCHHR8937bvQeihews0Df+QrsLAD\nwrfwfE8V8AeI72E2yHAX6LvFg7t2JFSlug==\n-----END EC PRIVATE KEY-----\n",
		UserSignCrt: "-----BEGIN CERTIFICATE-----\nMIICdTCCAhygAwIBAgIDCW/zMAoGCCqGSM49BAMCMIGKMQswCQYDVQQGEwJDTjEQ\nMA4GA1UECBMHQmVpamluZzEQMA4GA1UEBxMHQmVpamluZzEfMB0GA1UEChMWd3gt\nb3JnMS5jaGFpbm1ha2VyLm9yZzESMBAGA1UECxMJcm9vdC1jZXJ0MSIwIAYDVQQD\nExljYS53eC1vcmcxLmNoYWlubWFrZXIub3JnMB4XDTIzMTIwMTA4NDMxNFoXDTI4\nMTEyOTA4NDMxNFowgY8xCzAJBgNVBAYTAkNOMRAwDgYDVQQIEwdCZWlqaW5nMRAw\nDgYDVQQHEwdCZWlqaW5nMR8wHQYDVQQKExZ3eC1vcmcxLmNoYWlubWFrZXIub3Jn\nMQ4wDAYDVQQLEwVhZG1pbjErMCkGA1UEAxMiYWRtaW4xLnNpZ24ud3gtb3JnMS5j\naGFpbm1ha2VyLm9yZzBZMBMGByqGSM49AgEGCCqGSM49AwEHA0IABIcOni8rYIrW\nKWq5i/wC7+5Oxpwhx0fPd+270HooXsLNA3/kK7CwA8K38HxPFfAHiO9hNshwF+i7\nxYO7diRUpbqjajBoMA4GA1UdDwEB/wQEAwIGwDApBgNVHQ4EIgQgfs51BSgHvk4M\niDcCJPghxY3TGPUGIWuVWoZDyC37YtswKwYDVR0jBCQwIoAgD0avv9cED4/AJHlS\nMpfA+otHYKr8cuXrgvgxeVlblDcwCgYIKoZIzj0EAwIDRwAwRAIgE10Ns1fwCn1U\nfnDJnf0STF5Ipm36yscpvbBl0LPouQECIBs4UPGsU/gXlvzCGjYnCFGTfj4ny+F2\nuspvLrkFozei\n-----END CERTIFICATE-----\n",
	},
}

var (
	//onceMySQL      sync.Once
	onceClickHouse sync.Once
	onceRedis      sync.Once
	//mysqlCfg       *config.DBConf
	clickHouseCfg *config.DBConf
	redisCfg      *config.RedisConfig
)

func InitMySQLContainer() (*config.DBConf, error) {
	// var err error
	// onceMySQL.Do(func() {
	// 	//初始化配置
	// 	_ = config.InitConfig("", "")
	// 	//config.SubscribeChains = getChainListConfigTest("0_chainListConfig.json")
	// 	config.SubscribeChains = []*config.ChainInfo{
	// 		ChainListConfigTest,
	// 	}
	// 	// mysqlCfg, err = CreateMySQLContainer()
	// 	// if mysqlCfg == nil {
	// 	// 	return
	// 	// }
	// 	hostUrl := os.Getenv("WEAVIATE_URL")
	// 	mysqlCfg, err := parseDBURL(hostUrl)
	// 	log.Infof("=========mysqlCfg:%v====hostUrl:%v", mysqlCfg, hostUrl)
	// 	if err != nil {
	// 		return
	// 	}

	// 	config.GlobalConfig.DBConf = mysqlCfg
	// 	InitDbConn(mysqlCfg)
	// })

	//初始化配置
	_ = config.InitConfig("", "")
	//config.SubscribeChains = getChainListConfigTest("0_chainListConfig.json")
	config.SubscribeChains = []*config.ChainInfo{
		ChainListConfigTest,
	}
	// mysqlCfg, err = CreateMySQLContainer()
	// if mysqlCfg == nil {
	// 	return
	// }
	hostUrl := os.Getenv("WEAVIATE_URL")
	mysqlCfg, err := parseDBURL(hostUrl)
	log.Infof("=========mysqlCfg:%v====hostUrl:%v", mysqlCfg, hostUrl)
	if err != nil {
		return nil, err
	}

	config.GlobalConfig.DBConf = mysqlCfg
	InitDbConn(mysqlCfg)
	return mysqlCfg, err
}

func parseDBURL(dbURL string) (*config.DBConf, error) {
	// 使用正则表达式解析数据库连接字符串
	re := regexp.MustCompile(`^(?P<username>[^:]+):(?P<password>[^@]+)@` +
		`tcp\((?P<host>[^:]+):(?P<port>[0-9]+)\)(?:/(?P<database>.*))?$`)
	match := re.FindStringSubmatch(dbURL)
	if match == nil {
		return nil, fmt.Errorf("invalid database URL format")
	}

	// 创建一个 map 来存储解析后的值
	paramsMap := make(map[string]string)
	for i, name := range re.SubexpNames() {
		if i != 0 && name != "" {
			paramsMap[name] = match[i]
		}
	}

	// 填充配置结构体
	dbConf := &config.DBConf{
		Host:       paramsMap["host"],
		Port:       paramsMap["port"],
		Username:   paramsMap["username"],
		Password:   paramsMap["password"],
		Database:   paramsMap["database"],
		Prefix:     "test_",
		DbProvider: "Mysql",
	}
	return dbConf, nil
}

func CreateMySQLContainer() (*config.DBConf, error) {
	ctx := context.Background()
	req := testcontainers.ContainerRequest{
		Image:        "mysql:8.0",
		ExposedPorts: []string{"3306/tcp"},
		Env: map[string]string{
			"MYSQL_ROOT_PASSWORD": "password",
		},
		WaitingFor: wait.ForLog("ready for connections"), // 等待 MySQL 完成初始化
	}
	mysqlC, err := testcontainers.GenericContainer(ctx, testcontainers.GenericContainerRequest{
		ContainerRequest: req,
		Started:          true,
	})
	if err != nil {
		return nil, err
	}

	ip, err := mysqlC.Host(ctx)
	if err != nil {
		return nil, err
	}

	port, err := mysqlC.MappedPort(ctx, "3306")
	if err != nil {
		return nil, err
	}

	address := net.JoinHostPort(ip, port.Port())
	// 简单的连接检查
	for i := 0; i < 10; i++ {
		conn, err := net.Dial("tcp", address)
		if err == nil {
			log.Infof("====CreateMySQLContainer tcp test success====")
			conn.Close()
			break // 成功连接
		}
		log.Infof("====CreateMySQLContainer tcp test failed, Waiting for Retry====")
		time.Sleep(time.Second) // 等待再试
	}

	dbCfg := &config.DBConf{
		Host:       ip,
		Port:       port.Port(),
		Database:   "chainmaker_explorer_dev",
		Username:   "root",
		Password:   "password",
		Prefix:     "test_",
		DbProvider: "Mysql",
	}

	time.Sleep(20 * time.Second) // 等待 10 秒钟，确保数据库完全启动
	return dbCfg, nil
}

func InitClickHouseContainer() (*config.DBConf, error) {
	var err error
	onceClickHouse.Do(func() {
		//初始化配置
		_ = config.InitConfig("", "")
		clickHouseCfg, err = CreateClickHouseContainer()
		if clickHouseCfg == nil {
			return
		}
		config.GlobalConfig.DBConf = clickHouseCfg
		InitDbConn(clickHouseCfg)
	})
	config.GlobalConfig.DBConf = clickHouseCfg
	return clickHouseCfg, err
}

func CreateClickHouseContainer() (*config.DBConf, error) {
	ctx := context.Background()
	req := testcontainers.ContainerRequest{
		Image:        "yandex/clickhouse-server:latest",
		ExposedPorts: []string{"8123/tcp"},
		WaitingFor:   wait.ForLog("Listening for connections with native protocol").WithStartupTimeout(2 * time.Minute),
	}
	clickHouseC, err := testcontainers.GenericContainer(ctx, testcontainers.GenericContainerRequest{
		ContainerRequest: req,
		Started:          true,
	})
	if err != nil {
		return nil, err
	}

	ip, err := clickHouseC.Host(ctx)
	if err != nil {
		return nil, err
	}

	port, err := clickHouseC.MappedPort(ctx, "8123")
	if err != nil {
		return nil, err
	}

	dbCfg := &config.DBConf{
		Host:       ip,
		Port:       port.Port(),
		Database:   "chainmaker_explorer_dev",
		Username:   "root",
		Password:   "password",
		Prefix:     "test",
		DbProvider: "ClickHouse",
	}

	time.Sleep(time.Second * 10)
	return dbCfg, nil
}

func InitRedisContainer() (*config.RedisConfig, error) {
	var err error
	onceRedis.Do(func() {
		//初始化配置
		_ = config.InitConfig("", "")
		redisCfg, err = CreateRedisContainer()
	})
	config.GlobalConfig.RedisDB = redisCfg
	log.Infof("=========redisCfg:%v=", redisCfg)
	return redisCfg, err
}

func CreateRedisContainer() (*config.RedisConfig, error) {
	ctx := context.Background()
	req := testcontainers.ContainerRequest{
		Image:        "redis:latest",
		ExposedPorts: []string{"6379/tcp"},
		WaitingFor:   wait.ForLog("Ready to accept connections"),
	}
	redisC, err := testcontainers.GenericContainer(ctx, testcontainers.GenericContainerRequest{
		ContainerRequest: req,
		Started:          true,
	})
	if err != nil {
		return nil, err
	}

	ip, err := redisC.Host(ctx)
	if err != nil {
		return nil, err
	}

	port, err := redisC.MappedPort(ctx, "6379")
	if err != nil {
		return nil, err
	}

	address := fmt.Sprintf("%s:%s", ip, port.Port())
	// 使用 go-redis 客户端进行 PING 测试
	client := redis.NewClient(&redis.Options{
		Addr: address,
	})

	// 连接检查
	for i := 0; i < 10; i++ {
		pong, err := client.Ping(ctx).Result()
		if err == nil && pong == "PONG" {
			log.Infof("Redis PING test success")
			break
		}
		log.Infof("Redis PING test failed, Waiting for Retry")
		time.Sleep(time.Second) // 等待再试
	}

	time.Sleep(20 * time.Second) // 等待 10 秒钟，确保数据库完全启动

	redisCfg = &config.RedisConfig{
		Type:     "node",
		Host:     []string{ip + ":" + port.Port()},
		Password: "",
		Username: "",
	}
	return redisCfg, nil
}

func getChainListConfigTest(fileName string) []*config.ChainInfo {
	resultValue := make([]*config.ChainInfo, 0)
	// 打开 JSON 文件
	decoder, err := geFileData(fileName)
	if err != nil {
		fmt.Println("Error opening file:", err)
		return resultValue
	}
	err = decoder.Decode(&resultValue)
	if err != nil {
		fmt.Println("Error decoding JSON:", err)
		return resultValue
	}
	return resultValue
}

func geFileData(fileName string) (*json.Decoder, error) {
	file, err := os.Open("../testData/" + fileName)
	if err != nil {
		fmt.Println("Error opening file:", err)
		return nil, err
	}

	// 解码 JSON 文件内容到 blockInfo 结构体
	decoder := json.NewDecoder(file)
	return decoder, err
}
