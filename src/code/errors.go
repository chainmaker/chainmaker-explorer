// Package code 定义错误码
package code

const (
	// ErrorContractCompilerTimeout
	ErrorContractCompilerTimeout = "源码编译超时，请重新尝试"
	// ErrorContractCompilerFailed
	ErrorContractCompilerFailed = "源码编译失败"
	// ErrorContractCompilerDiff
	ErrorContractCompilerDiff = "合约编译文件比对不成功"
)
