package sync

import (
	"chainmaker_web/src/db"
	"chainmaker_web/src/sync/model"
	"testing"

	pbCommon "chainmaker.org/chainmaker/pb-go/v2/common"
)

func Test_executeDataInsertTasks(t *testing.T) {
	type args struct {
		chainId    string
		dealResult model.ProcessedBlockData
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "Test case 1",
			args: args{
				chainId: ChainId1,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := executeDataInsertTasks(tt.args.chainId, tt.args.dealResult); (err != nil) != tt.wantErr {
				t.Errorf("executeDataInsertTasks() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestRealtimeDataSaveToDB(t *testing.T) {
	crossCycleTx := &db.CrossCycleTransaction{
		CrossId:   "1234",
		StartTime: 123,
		EndTime:   456,
	}

	dealResult := model.ProcessedBlockData{
		BlockDetail: &db.Block{
			BlockHeight: 1,
			BlockHash:   "abc123",
		},
		UserList: map[string]*db.User{
			"user1": {
				UserId:   "user1",
				UserAddr: "address1",
				Role:     "admin",
			},
		},
		Transactions: map[string]*db.Transaction{
			"tx1": {
				TxId:        "tx1",
				Sender:      "user1",
				BlockHeight: 1,
				BlockHash:   "abc123",
				TxType:      "type1",
			},
		},
		CrossChainResult: &model.CrossChainResult{
			SaveCrossCycleTx: map[string]*db.CrossCycleTransaction{
				"1234": crossCycleTx,
			},
			UpdateCrossCycleTx: map[string]*db.CrossCycleTransaction{},
			InsertCrossCycleTx: []*db.CrossCycleTransaction{},
		},
	}

	type args struct {
		chainId     string
		blockHeight int64
		dealResult  model.ProcessedBlockData
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "Test case 1",
			args: args{
				chainId:     ChainId1,
				blockHeight: 12,
				dealResult:  dealResult,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			//todo 1111111111111
			// if err := RealtimeDataSaveToDB(tt.args.chainId, tt.args.blockHeight, tt.args.dealResult, tt.args.txTimeLog); (err != nil) != tt.wantErr {
			// 	t.Errorf("RealtimeDataSaveToDB() error = %v, wantErr %v", err, tt.wantErr)
			// }
		})
	}
}

func TestProcessedBlockInfo_ProcessedBlockHandle(t *testing.T) {
	blockInfo := getBlockInfoTest("1_blockInfoJsonContract.json")
	type args struct {
		BlockInfo *pbCommon.BlockInfo
		HashType  string
	}
	tests := []struct {
		name    string
		args    args
		want    *model.ProcessedBlockData
		wantErr bool
	}{
		{
			name: "Test case 1",
			args: args{
				BlockInfo: blockInfo,
				HashType:  "123",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			block := &ProcessedBlockInfo{
				BlockInfo: tt.args.BlockInfo,
				HashType:  tt.args.HashType,
			}
			_, err := block.ProcessedBlockHandle()
			if (err != nil) != tt.wantErr {
				t.Errorf("ProcessedBlockInfo.ProcessedBlockHandle() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			// if !reflect.DeepEqual(got, tt.want) {
			// 	t.Errorf("ProcessedBlockInfo.ProcessedBlockHandle() = %v, want %v", got, tt.want)
			// }
		})
	}
}
