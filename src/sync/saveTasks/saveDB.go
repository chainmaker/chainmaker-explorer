/*
Package saveTasks comment： resolver realtime inset DB
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
SPDX-License-Identifier: Apache-2.0
*/
package saveTasks

import (
	"chainmaker_web/src/db"
	"chainmaker_web/src/db/dbhandle"
	loggers "chainmaker_web/src/logger"
)

var (
	log = loggers.GetLogger(loggers.MODULE_WEB)
)

const (
	GoRoutinePoolErr = "new ants pool error: "
)

// SaveAccountToDB SaveAccount
func SaveAccountToDB(chainId string, accountResult *db.UpdateAccountResult) error {
	insertAccounts := accountResult.InsertAccount
	updateAccounts := accountResult.UpdateAccount
	//插入账户
	err := dbhandle.InsertAccount(chainId, insertAccounts)
	if err != nil {
		return err
	}

	//更新账户
	for _, account := range updateAccounts {
		err = dbhandle.UpdateAccount(chainId, account)
		if err != nil {
			return err
		}
	}
	return err
}
