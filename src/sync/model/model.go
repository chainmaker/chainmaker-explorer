/*
Package sync comment
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
SPDX-License-Identifier: Apache-2.0
*/
package model

import (
	"chainmaker_web/src/db"

	pbConfig "chainmaker.org/chainmaker/pb-go/v2/config"
)

// TxTimeLog 处理时间日志
// type TxTimeLog struct {
// 	TimeStart              int64
// 	TimeContractEvents     int64
// 	TimeRealtimeDataHandle int64
// 	TimeRealtimeSaveToDB   int64
// 	TimeParseTransactions  int64
// 	TimeParseContracts     int64
// 	TimeDealBlock          int64
// }

// // RealtimeDealResult 区块数据格式化后结果
// type RealtimeDealResult struct {
// 	//BlockDetail 区块数据
// 	BlockDetail *db.Block
// 	//UserList 用户数据
// 	UserList map[string]*db.User
// 	//Transactions 交易数据
// 	Transactions map[string]*db.Transaction
// 	//UpgradeContractTransaction 合约版本交易
// 	UpgradeContractTx []*db.UpgradeContractTransaction
// 	//ChainConfigList 链配置数据
// 	ChainConfigList []*pbConfig.ChainConfig
// 	//InsertContracts 新增合约
// 	ContractWriteSetData map[string]*ContractWriteSetData
// 	InsertContracts      []*db.Contract
// 	//UpdateContracts 修改合约
// 	UpdateContracts []*db.Contract
// 	//FungibleContract
// 	FungibleContract []*db.FungibleContract
// 	//NonFungibleContract
// 	NonFungibleContract []*db.NonFungibleContract
// 	InsertIDAContracts  []*db.IDAContract
// 	//EvidenceList 存证合约
// 	EvidenceList []*db.EvidenceContract
// 	//ContractEvents 合约事件
// 	ContractEvents []*db.ContractEvent
// 	//GasRecordList gas消耗列表
// 	GasRecordList []*db.GasRecord
// 	//CrossChainResult 主子链相关数据
// 	CrossChainResult *CrossChainResult
// }

// ProcessedBlockData 表示经过区块链同步处理后的区块数据
type ProcessedBlockData struct {
	BlockDetail          *db.Block                        // 区块详细信息
	UserList             map[string]*db.User              // 用户列表
	Transactions         map[string]*db.Transaction       // 交易数据
	UpgradeContractTx    []*db.UpgradeContractTransaction // 合约升级交易
	ChainConfigList      []*pbConfig.ChainConfig          // 链配置列表
	ContractWriteSetData map[string]*ContractWriteSetData // 合约写集数据
	InsertContracts      []*db.Contract                   // 新增合约
	UpdateContracts      []*db.Contract                   // 更新合约
	FungibleContract     []*db.FungibleContract           // 同质化合约
	NonFungibleContract  []*db.NonFungibleContract        // 非同质化合约
	InsertIDAContracts   []*db.IDAContract                // 新增 IDA 合约
	EvidenceList         []*db.EvidenceContract           // 存证合约列表
	ContractEvents       []*db.ContractEvent              // 合约事件
	GasRecordList        []*db.GasRecord                  // gas 消耗记录
	CrossChainResult     *CrossChainResult                // 跨链相关数据
	Timestamp            int64
}

// NewProcessedBlockData new一个pool
func NewProcessedBlockData() *ProcessedBlockData {
	return &ProcessedBlockData{
		BlockDetail:          &db.Block{},
		UserList:             map[string]*db.User{},
		Transactions:         map[string]*db.Transaction{},
		UpgradeContractTx:    make([]*db.UpgradeContractTransaction, 0),
		ChainConfigList:      make([]*pbConfig.ChainConfig, 0),
		ContractWriteSetData: map[string]*ContractWriteSetData{},
		InsertContracts:      make([]*db.Contract, 0),
		UpdateContracts:      make([]*db.Contract, 0),
		FungibleContract:     make([]*db.FungibleContract, 0),
		NonFungibleContract:  make([]*db.NonFungibleContract, 0),
		InsertIDAContracts:   make([]*db.IDAContract, 0),
		EvidenceList:         make([]*db.EvidenceContract, 0),
		ContractEvents:       make([]*db.ContractEvent, 0),
		GasRecordList:        make([]*db.GasRecord, 0),
		CrossChainResult:     &CrossChainResult{},
	}
}

// CrossChainSaveDB 跨链主子链数据
type CrossChainSaveDB struct {
	//InsertSubChainList 新增子链
	InsertSubChainList []*db.CrossSubChainData
	//UpdateSubChainList 更新子链
	UpdateSubChainList []*db.CrossSubChainData
	//SubChainBlockHeight 需要更新的子链高度列表
	SubChainBlockHeight map[string]int64
}

// GetRealtimeCacheData 区块同步缓存数据，供异步更新计算使用
type GetRealtimeCacheData struct {
	TxList         map[string]*db.Transaction
	ContractAddrs  map[string]string
	GasRecords     []*db.GasRecord
	ContractEvents []*db.ContractEvent
	CrossTransfers []*db.CrossTransactionTransfer
	UserInfoMap    map[string]*db.User
}

// NewGetRealtimeCacheData 初始化GetRealtimeCacheData
func NewGetRealtimeCacheData() *GetRealtimeCacheData {
	return &GetRealtimeCacheData{
		TxList:         map[string]*db.Transaction{},
		ContractAddrs:  map[string]string{},
		GasRecords:     []*db.GasRecord{},
		ContractEvents: []*db.ContractEvent{},
		CrossTransfers: []*db.CrossTransactionTransfer{},
		UserInfoMap:    map[string]*db.User{},
	}
}

// ContractWriteSetData 读写集解析合约数据
type ContractWriteSetData struct {
	ContractName     string
	ContractNameBak  string
	ContractSymbol   string
	ContractAddr     string
	ContractType     string
	ContractByteCode []byte
	Version          string
	RuntimeType      string
	ContractStatus   int32
	BlockHeight      int64
	OrgId            string
	SenderTxId       string
	Sender           string
	SenderAddr       string
	Timestamp        int64
	Decimals         int
}

// DelayedUpdateData 异步计算存储数据
type DelayedUpdateData struct {
	InsertSubChainCross []*db.CrossSubChainCrossChain
	UpdateSubChainCross []*db.CrossSubChainCrossChain
	UpdateSubChainData  []*db.CrossSubChainData
	InsertGasList       []*db.Gas
	UpdateGasList       []*db.Gas
	UpdateTxBlack       *db.UpdateTxBlack
	ContractResult      *db.GetContractResult
	FungibleTransfer    []*db.FungibleTransfer
	NonFungibleTransfer []*db.NonFungibleTransfer
	BlockPosition       *db.BlockPosition
	UpdateAccountResult *db.UpdateAccountResult
	TokenResult         *db.TokenResult
	ContractMap         map[string]*db.Contract
	IDAInsertAssetsData *db.IDAAssetsDataDB
	IDAUpdateAssetsData *db.IDAAssetsUpdateDB
}

// GetDBResult 需要用到的数据库数据
type GetDBResult struct {
	GasList                []*db.Gas
	PositionMapList        map[string][]*db.FungiblePosition
	NonPositionMapList     map[string][]*db.NonFungiblePosition
	FungibleContractMap    map[string]*db.FungibleContract
	NonFungibleContractMap map[string]*db.NonFungibleContract
	AddBlackTxList         []*db.Transaction
	DeleteBlackTxList      []*db.BlackTransaction
	CrossSubChainCross     []*db.CrossSubChainCrossChain
	CrossSubChainMap       map[string]*db.CrossSubChainData
	AccountBNSList         []*db.Account
	AccountDIDList         []*db.Account
	AccountDBMap           map[string]*db.Account
	IDAContractMap         map[string]*db.IDAContract
	IDAAssetDetailMap      map[string]*db.IDAAssetDetail
	EventTopicMap          map[string]map[string]*db.ContractEventTopic
}

type BatchDelayedUpdateLog struct {
	GetRealtimeCacheTime      int64
	DelayedUpdateDataTime     int64
	UpdateDataToDBTime        int64
	UpdateBlockStatusToDBTime int64
}

type TopicEventResult struct {
	AddBlack          []string
	DeleteBlack       []string
	IdentityContract  []*db.IdentityContract
	ContractEventData []*db.ContractEventData
	OwnerAdders       []string
	DIDAccount        map[string][]string
	DIDUnBindList     []string
	BNSBindEventData  []*db.BNSTopicEventData
	BNSUnBindDomain   []string
	IDAEventData      *IDAEventData
}

type IDAEventData struct {
	IDACreatedMap     map[string][]*db.IDACreatedInfo
	IDAUpdatedMap     map[string][]*db.EventIDAUpdatedData
	IDADeletedCodeMap map[string][]string
	EventTime         int64
}

// CrossChainResult 跨链主子链数据
type CrossChainResult struct {
	//SaveSubChainList 保存子链数据
	SaveSubChainList []*db.CrossSubChainData
	//CrossMainTransaction 跨链交易主链交易
	CrossMainTransaction []*db.CrossMainTransaction
	//CrossTransfer 跨链交易流转数据
	CrossTransfer      map[string]*db.CrossTransactionTransfer
	InsertCrossCycleTx []*db.CrossCycleTransaction
	SaveCrossCycleTx   map[string]*db.CrossCycleTransaction
	UpdateCrossCycleTx map[string]*db.CrossCycleTransaction
	//BusinessTxMap 跨链交易-具体业务交易
	BusinessTxMap map[string]*db.CrossBusinessTransaction
	//SubChainBlockHeight 跨链子链高度
	SubChainBlockHeight map[string]int64
	//CrossChainContractMap 跨链合约数据
	CrossChainContractMap map[string]map[string]string
	//GateWayIds 跨链网关
	GateWayIds []int64
}

func NewCrossChainResult() *CrossChainResult {
	return &CrossChainResult{
		SaveSubChainList:      make([]*db.CrossSubChainData, 0),
		CrossMainTransaction:  make([]*db.CrossMainTransaction, 0),
		CrossTransfer:         make(map[string]*db.CrossTransactionTransfer),
		InsertCrossCycleTx:    make([]*db.CrossCycleTransaction, 0),
		SaveCrossCycleTx:      make(map[string]*db.CrossCycleTransaction),
		UpdateCrossCycleTx:    make(map[string]*db.CrossCycleTransaction),
		BusinessTxMap:         make(map[string]*db.CrossBusinessTransaction),
		SubChainBlockHeight:   make(map[string]int64),
		CrossChainContractMap: make(map[string]map[string]string),
		GateWayIds:            make([]int64, 0),
	}
}
