package logic

import (
	"chainmaker_web/src/db"
	"chainmaker_web/src/db/dbhandle"
	"chainmaker_web/src/sync/common"
	"chainmaker_web/src/sync/model"
	"encoding/base64"
	"encoding/json"
	"strings"
	"sync"

	pbCommon "chainmaker.org/chainmaker/pb-go/v2/common"
	pbConfig "chainmaker.org/chainmaker/pb-go/v2/config"
	tcipCommon "chainmaker.org/chainmaker/tcip-go/v2/common"
	"github.com/gogo/protobuf/proto"
	"github.com/panjf2000/ants/v2"
)

// CrossContract 跨链合约
type CrossContract struct {
	SubChainId   string
	ContractName string
}

// ParallelParseWriteSetData
// @Description: 并发解析读写集数据，获取链配置，主子链数据
// @param blockInfo 区块信息
// @param dealResult 处理结果
// @return error 错误信息
func ParallelParseWriteSetData(blockInfo *pbCommon.BlockInfo, dealResult *model.ProcessedBlockData) error {
	//主子链数据
	crossResult := model.NewCrossChainResult()
	// 使用同步互斥锁保护共享资源
	var mutx sync.Mutex
	var wg sync.WaitGroup

	// 创建一个固定大小的 goroutine 池
	goRoutinePool, err := ants.NewPool(10, ants.WithPreAlloc(false))
	if err != nil {
		log.Errorf("Failed to create goroutine pool: %v", err)
		return err
	}
	defer goRoutinePool.Release()

	chainId := blockInfo.Block.Header.ChainId
	blockHeight := int64(blockInfo.Block.Header.BlockHeight)
	// 用来接收并发任务的错误，减少通道的容量，避免阻塞
	errChan := make(chan error, 10)

	for i, tx := range blockInfo.Block.Txs {
		wg.Add(1)
		errSub := goRoutinePool.Submit(func(i int, blockInfo *pbCommon.BlockInfo, txInfo *pbCommon.Transaction) func() {
			return func() {
				defer wg.Done()
				//读集
				// 确保 RwsetList 有足够的长度
				if i >= len(blockInfo.RwsetList) {
					log.Errorf("Index out of range: i=%d, RwsetList length=%d", i, len(blockInfo.RwsetList))
					return
				}

				rwSetList := blockInfo.RwsetList[i]

				// 处理其他写集数据，包括链配置数据，主子链数据等
				if errOther := processWriteSetDataOther(&mutx, rwSetList, txInfo, dealResult); errOther != nil {
					errChan <- errOther
				}

				// 处理跨链写集数据，包括跨链交易数据，跨链合约数据等
				if errCross := processCrossChainTransaction(&mutx, rwSetList, txInfo, chainId,
					blockHeight, crossResult); errCross != nil {
					errChan <- errCross
				}
			}
		}(i, blockInfo, tx))
		if errSub != nil {
			log.Error("ParallelParseWriteSetData submit Failed: " + errSub.Error())
			wg.Done() // 减少 WaitGroup 计数
		}
	}

	wg.Wait()
	if len(errChan) > 0 {
		err = <-errChan
		return err
	}

	dealResult.CrossChainResult = crossResult
	return nil
}

// processWriteSetDataOther 处理其他写集数据
// @param mutx 互斥锁
// @param rwSetList 读写集列表
// @param txInfo 交易信息
// @param dealResult 处理结果
// @return error 错误信息
func processWriteSetDataOther(mutx *sync.Mutex, rwSetList *pbCommon.TxRWSet, txInfo *pbCommon.Transaction,
	dealResult *model.ProcessedBlockData) error {
	// 根据写集解析链配置数据
	chainConfig, err := GetChainConfigByWriteSet(rwSetList, txInfo)
	if err != nil {
		return err
	}

	// 锁定操作，避免并发修改
	mutx.Lock()
	defer mutx.Unlock()
	// 修改链配置
	if chainConfig != nil && chainConfig.ChainId != "" {
		dealResult.ChainConfigList = append(dealResult.ChainConfigList, chainConfig)
	}

	return nil
}

// processTransaction 处理跨链交易
// @param mutx 互斥锁
// @param rwSetList 读写集列表
// @param txInfo 交易信息
// @param chainId 链ID
// @param blockHeight 区块高度，用于版本控制
// @param crossResult 跨链结果
// @return error 错误信息
func processCrossChainTransaction(mutx *sync.Mutex, rwSetList *pbCommon.TxRWSet, txInfo *pbCommon.Transaction,
	chainId string, blockHeight int64, crossResult *model.CrossChainResult) error {
	var (
		mainTransaction     *db.CrossMainTransaction
		saveCycleTx         *db.CrossCycleTransaction
		updateCycleTx       *db.CrossCycleTransaction
		crossChainContracts []*CrossContract
	)
	crossTxTransferList := make([]*db.CrossTransactionTransfer, 0)
	businessTxMap := make(map[string]*db.CrossBusinessTransaction)

	// 根据写集解析跨链交易数据
	crossChainInfo, crossErr := GetCrossChainInfoByWriteSet(rwSetList, txInfo)
	if crossErr != nil {
		return crossErr
	}

	// 处理跨链交易
	if crossChainInfo != nil && crossChainInfo.CrossChainId != "" {
		mainTransaction, crossTxTransferList, businessTxMap, saveCycleTx, updateCycleTx,
			crossChainContracts = processCrossChainInfo(chainId, blockHeight, crossChainInfo, txInfo)
	}

	// 解析其他跨链数据
	subBlockHeight, spvContractName := GetSubChainBlockHeightByWriteSet(rwSetList, txInfo)
	gateWayId := GetSubChainGatewayIdByWriteSet(rwSetList, txInfo)

	// 锁定操作，避免并发修改
	mutx.Lock()
	defer mutx.Unlock()

	//更新跨链交易
	UpdateCrossChainResultTx(crossResult, mainTransaction, crossTxTransferList, saveCycleTx, updateCycleTx, businessTxMap)
	//更新跨链其他信息
	UpdateCrossChainResult(crossResult, gateWayId, subBlockHeight, spvContractName, crossChainContracts)
	return nil
}

// UpdateCrossChainResult 更新跨链结果
// @param crossResult 跨链结果
// @param gateWayId 子链网关
// @param subBlockHeight 子链高度
// @param spvContractName 子链合约
// @param crossChainContracts 跨链合约
// @return error 错误信息
func UpdateCrossChainResult(crossResult *model.CrossChainResult, gateWayId *int64, subBlockHeight int64,
	spvContractName string, crossChainContracts []*CrossContract) {
	// 跨链-子链网关
	if gateWayId != nil {
		crossResult.GateWayIds = append(crossResult.GateWayIds, *gateWayId)
	}

	// 跨链-子链高度
	if subBlockHeight > 0 && spvContractName != "" {
		if height, ok := crossResult.SubChainBlockHeight[spvContractName]; ok {
			if subBlockHeight > height {
				crossResult.SubChainBlockHeight[spvContractName] = subBlockHeight
			}
		} else {
			crossResult.SubChainBlockHeight[spvContractName] = subBlockHeight
		}
	}

	// 跨链合约
	if len(crossChainContracts) > 0 {
		for _, crossContract := range crossChainContracts {
			subChain := crossContract.SubChainId
			subConName := crossContract.ContractName
			if crossResult.CrossChainContractMap[subChain] == nil {
				crossResult.CrossChainContractMap[subChain] = make(map[string]string)
			}
			crossResult.CrossChainContractMap[subChain][subConName] = subConName
		}
	}
}

// UpdateCrossChainResultTx 更新跨链交易结果
// @param crossResult 跨链结果
// @param mainTransaction 主交易
// @param crossTxTransferList 跨链交易流转
// @param saveCycleTx 保存周期交易
// @param updateCycleTx 更新周期交易
// @param businessTxMap 业务交易
// @return error 错误信息
func UpdateCrossChainResultTx(crossResult *model.CrossChainResult, mainTransaction *db.CrossMainTransaction,
	crossTxTransferList []*db.CrossTransactionTransfer, saveCycleTx, updateCycleTx *db.CrossCycleTransaction,
	businessTxMap map[string]*db.CrossBusinessTransaction) {
	// 跨链交易
	if mainTransaction != nil {
		crossResult.CrossMainTransaction = append(crossResult.CrossMainTransaction, mainTransaction)
	}

	// 跨链交易流转
	if len(crossTxTransferList) > 0 {
		for _, transfer := range crossTxTransferList {
			mapKey := transfer.CrossId + "_" + transfer.FromChainId + "_" + transfer.ToChainId
			if _, ok := crossResult.CrossTransfer[mapKey]; !ok {
				crossResult.CrossTransfer[mapKey] = transfer
			}
		}
	}

	//更新跨链交易状态
	if saveCycleTx != nil {
		crossId := saveCycleTx.CrossId
		if cycleTx, ok := crossResult.SaveCrossCycleTx[crossId]; ok {
			//是否是更新的状态，如果是换成最新数据
			if saveCycleTx.Status > cycleTx.Status {
				saveCycleTx.StartTime = cycleTx.StartTime
			}
			isEnd := common.IsCrossEnd(saveCycleTx.Status)
			if isEnd {
				saveCycleTx.Duration = saveCycleTx.EndTime - saveCycleTx.StartTime
			}
		}
		crossResult.SaveCrossCycleTx[crossId] = saveCycleTx
	}
	//更新跨链交易状态
	if updateCycleTx != nil {
		crossId := updateCycleTx.CrossId
		if cycleTx, ok := crossResult.UpdateCrossCycleTx[crossId]; ok {
			//是否是更新的状态，如果是换成最新数据
			if updateCycleTx.Status > cycleTx.Status {
				crossResult.UpdateCrossCycleTx[crossId] = updateCycleTx
			}
		} else {
			crossResult.UpdateCrossCycleTx[crossId] = updateCycleTx
		}
	}

	// 跨链-具体执行的交易
	if len(businessTxMap) > 0 {
		for mapKey, executionTx := range businessTxMap {
			if _, ok := crossResult.BusinessTxMap[mapKey]; !ok {
				crossResult.BusinessTxMap[mapKey] = executionTx
			}
		}
	}
}

// processCrossChainInfo
//
//	@Description: 根据区块跨链数据crossChainInfo，解析主子链数据
//	@param chainId 链ID
//	@param blockHeight 链高度
//	@param crossChainInfo 主子链区块数据
//	@param txInfo 交易数据
//	@return *db.CrossMainTransaction z主子链:主链交易
//	@return []*db.CrossTransactionTransfer 主子链交易流转数据
//	@return map[string]*db.CrossBusinessTransaction 主子链具体任务交易
//	@return *db.CrossCycleTransaction 主子链跨链周期数据
//	@return []*CrossContract 主子链跨链合约数据
func processCrossChainInfo(chainId string, blockHeight int64, crossChainInfo *tcipCommon.CrossChainInfo,
	txInfo *pbCommon.Transaction) (*db.CrossMainTransaction, []*db.CrossTransactionTransfer,
	map[string]*db.CrossBusinessTransaction, *db.CrossCycleTransaction, *db.CrossCycleTransaction, []*CrossContract) {
	var (
		mainTransaction     *db.CrossMainTransaction
		insertCycleTx       *db.CrossCycleTransaction
		updateCycleTx       *db.CrossCycleTransaction
		crossChainContracts []*CrossContract
		crossTxTransferList []*db.CrossTransactionTransfer
		businessTxMap       map[string]*db.CrossBusinessTransaction
	)

	//存在跨链交易
	if crossChainInfo == nil || crossChainInfo.CrossChainId == "" {
		return mainTransaction, crossTxTransferList, businessTxMap, insertCycleTx, updateCycleTx, crossChainContracts
	}

	timestamp := txInfo.Payload.Timestamp
	crossId := crossChainInfo.CrossChainId
	//判断是否已经存在，存在就不在重复解析
	crossTxInfo, _ := dbhandle.GetCrossCycleById(chainId, crossId)
	if crossTxInfo == nil {
		//跨链-主子链交易流转信息
		crossTxTransferList = GetCrossTxTransfer(chainId, blockHeight, crossChainInfo)
		//跨链合约
		for _, chainMsg := range crossChainInfo.CrossChainMsg {
			crossChainContracts = append(crossChainContracts, &CrossContract{
				SubChainId:   chainMsg.ChainRid,
				ContractName: chainMsg.ContractName,
			})
		}
		//开始，更新跨链周期数据
		insertCycleTx = GetCrossCycleInsertTx(crossChainInfo, blockHeight, timestamp)
	} else {
		//开始，更新跨链周期数据
		updateCycleTx = GetCrossCycleTransaction(crossChainInfo, crossTxInfo, blockHeight, timestamp)
	}

	//跨链-主链交易
	mainTransaction = GetMainCrossTransaction(crossChainInfo, txInfo)

	//跨链-具体执行的交易数据
	businessTxMap = GetBusinessTransaction(chainId, crossChainInfo)
	return mainTransaction, crossTxTransferList, businessTxMap, insertCycleTx, updateCycleTx, crossChainContracts
}

// GetChainConfigByWriteSet
//
//	@Description: 通过读写集解析链配置
//	@param txRWSet
//	@param txInfo
//	@return *pbConfig.ChainConfig
//	@return error
func GetChainConfigByWriteSet(txRWSet *pbCommon.TxRWSet, txInfo *pbCommon.Transaction) (*pbConfig.ChainConfig, error) {
	chainConfig := &pbConfig.ChainConfig{}
	//是否配置类交易
	isConfigTx := common.IsConfigTx(txInfo)
	if !isConfigTx || txRWSet == nil {
		return nil, nil
	}

	for _, write := range txRWSet.TxWrites {
		if string(write.Key) == common.TxReadWriteKeyChainConfig {
			err := proto.Unmarshal(write.Value, chainConfig)
			if err != nil {
				return nil, err
			}
			break
		}
	}
	if chainConfig.ChainId != "" {
		return chainConfig, nil
	}
	return nil, nil
}

// GetCrossChainInfoByWriteSet
//
//	@Description: 通过读写集解析跨链数据
//	@param txRWSet
//	@param txInfo
func GetCrossChainInfoByWriteSet(txRWSet *pbCommon.TxRWSet, txInfo *pbCommon.Transaction) (
	*tcipCommon.CrossChainInfo, error) {
	crossChainInfo := &tcipCommon.CrossChainInfo{}
	//是否跨链类交易
	isCrossChainTx := common.IsRelayCrossChainTx(txInfo)
	if !isCrossChainTx || txRWSet == nil {
		return nil, nil
	}

	for _, write := range txRWSet.TxWrites {
		//以c开头的key可以解析成CrossChainInfo
		if strings.HasPrefix(string(write.Key), "c") {
			// Base64 解码
			decoded, err := base64.StdEncoding.DecodeString(string(write.Value))
			if err != nil {
				return nil, err
			}
			err = json.Unmarshal(decoded, crossChainInfo)
			if err != nil {
				return nil, err
			}
			break
		}
	}
	if crossChainInfo.CrossChainId != "" {
		return crossChainInfo, nil
	}
	return nil, nil
}

// GetSubChainBlockHeightByWriteSet
//
//	@Description:  通过读写集解析跨链子链区块高度，bh开头的key表示区块高度
//	@param txRWSet
//	@param txInfo
//	@return int64
//	@return string
func GetSubChainBlockHeightByWriteSet(txRWSet *pbCommon.TxRWSet, txInfo *pbCommon.Transaction) (int64, string) {
	//是否跨链类交易
	isSubChainSpvTx, contractName := common.IsSubChainSpvContractTx(txInfo)
	if !isSubChainSpvTx || txRWSet == nil {
		return 0, contractName
	}

	var maxBlockHeight int64
	for _, write := range txRWSet.TxWrites {
		//以c开头的key可以解析成CrossChainInfo
		if strings.HasPrefix(string(write.Key), "bh") {
			blockHeight := common.BlockHeightExtractNumber(string(write.Key))
			if blockHeight > maxBlockHeight {
				maxBlockHeight = blockHeight
			}
		}
	}

	return maxBlockHeight, contractName
}

// GetSubChainGatewayIdByWriteSet
//
//	@Description: 读写集解析子链gateWay，g开头的key
//	@param txRWSet
//	@param txInfo
//	@return *int64
func GetSubChainGatewayIdByWriteSet(txRWSet *pbCommon.TxRWSet, txInfo *pbCommon.Transaction) *int64 {
	//是否跨链类交易
	isCrossChainTx := common.IsRelayCrossChainTx(txInfo)
	if !isCrossChainTx || txRWSet == nil {
		return nil
	}

	for _, write := range txRWSet.TxWrites {
		//以g开头的key可以解析成GatewayId
		if strings.HasPrefix(string(write.Key), "g") {
			gatewayId := common.GatewayIdExtractNumber(string(write.Key))
			return &gatewayId
		}
	}

	return nil
}
