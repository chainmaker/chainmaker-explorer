/*
Package sync comment
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
SPDX-License-Identifier: Apache-2.0
*/
package logic

import (
	"chainmaker_web/src/db"
	"chainmaker_web/src/sync/common"
	"chainmaker_web/src/sync/model"
	"encoding/json"

	"github.com/google/uuid"

	"chainmaker.org/chainmaker/contract-utils/standard"
	pbCommon "chainmaker.org/chainmaker/pb-go/v2/common"
)

// 创建一个新的TopicEventResult结构体
func newTopicEventResult() *model.TopicEventResult {
	return &model.TopicEventResult{
		//AddBlack 添加黑名单交易
		AddBlack: make([]string, 0),
		//DeleteBlack 删除黑名单交易
		DeleteBlack: make([]string, 0),
		//IdentityContract 身份合约
		IdentityContract: make([]*db.IdentityContract, 0),
		//ContractEventData 合约事件
		ContractEventData: make([]*db.ContractEventData, 0),
		//OwnerAdders 合约拥有者
		OwnerAdders: make([]string, 0),
		//DIDAccount DID账户
		DIDAccount: make(map[string][]string, 0),
		//DIDUnBindList DID解绑列表
		DIDUnBindList: make([]string, 0),
		//BNSBindEventData BNS绑定事件
		BNSBindEventData: make([]*db.BNSTopicEventData, 0),
		//BNSUnBindDomain BNS解绑域名
		BNSUnBindDomain: make([]string, 0),
	}
}

// 创建一个新的IDAEventResult结构体
func newIDAEventResult() *model.IDAEventData {
	return &model.IDAEventData{
		//IDACreatedMap 创建IDA合约
		IDACreatedMap: make(map[string][]*db.IDACreatedInfo),
		//IDAUpdatedMap 更新IDA合约
		IDAUpdatedMap: make(map[string][]*db.EventIDAUpdatedData),
		//IDADeletedCodeMap 删除IDA合约
		IDADeletedCodeMap: make(map[string][]string),
	}
}

// DealContractEvents 解析所有合约事件
// @param txInfo 交易信息
// @return 合约事件
func DealContractEvents(txInfo *pbCommon.Transaction) []*db.ContractEvent {
	contractEvents := make([]*db.ContractEvent, 0)
	//失败的操作不处理
	if txInfo.Result.ContractResult == nil ||
		txInfo.Result.ContractResult.Code != 0 {
		return contractEvents
	}

	resEvent := txInfo.Result.ContractResult.ContractEvent
	// 处理合约交易事件
	for i, event := range resEvent {
		eventDataJson, _ := json.Marshal(event.EventData)
		newUUID := uuid.New().String()
		contractEvent := &db.ContractEvent{
			ID:              newUUID,
			Topic:           event.Topic,
			EventIndex:      i + 1,
			TxId:            event.TxId,
			ContractName:    event.ContractName,
			ContractNameBak: event.ContractName,
			ContractVersion: event.ContractVersion,
			EventData:       string(eventDataJson),
			Timestamp:       txInfo.Payload.Timestamp,
		}
		contractEvents = append(contractEvents, contractEvent)
	}
	return contractEvents
}

func parseEventData(event *db.ContractEvent) []string {
	var eventData []string
	if event.EventDataBak != "" {
		_ = json.Unmarshal([]byte(event.EventDataBak), &eventData)
	} else if event.EventData != "" {
		_ = json.Unmarshal([]byte(event.EventData), &eventData)
	}
	return eventData
}

func processIDAEvent(event *db.ContractEvent, eventData []string, idaEventResult *model.IDAEventData) {
	idaCreatedMap := idaEventResult.IDACreatedMap
	idaUpdatedMap := idaEventResult.IDAUpdatedMap
	idaDeletedCodeMap := idaEventResult.IDADeletedCodeMap
	idaInfos, idaUpdateData, idaDeleteIds := BuildIDAEventData(event.ContractType, event.Topic, eventData)
	if len(idaInfos) > 0 {
		createdInfos := make([]*db.IDACreatedInfo, 0)
		for _, idaInfo := range idaInfos {
			createdInfo := &db.IDACreatedInfo{
				IDAInfo:      idaInfo,
				ContractAddr: event.ContractAddr,
				EventTime:    event.Timestamp,
			}
			createdInfos = append(createdInfos, createdInfo)
		}

		// 检查并初始化切片
		if _, ok := idaCreatedMap[event.ContractAddr]; !ok {
			idaCreatedMap[event.ContractAddr] = make([]*db.IDACreatedInfo, 0)
		}
		idaCreatedMap[event.ContractAddr] = append(idaCreatedMap[event.ContractAddr], createdInfos...)
	}

	if idaUpdateData != nil {
		idaUpdateData.EventTime = event.Timestamp
		if _, ok := idaUpdatedMap[idaUpdateData.IDACode]; !ok {
			idaUpdatedMap[idaUpdateData.IDACode] = make([]*db.EventIDAUpdatedData, 0)
		}
		idaUpdatedMap[idaUpdateData.IDACode] = append(idaUpdatedMap[idaUpdateData.IDACode], idaUpdateData)
	}

	if len(idaDeleteIds) > 0 {
		// 检查并初始化切片
		if _, ok := idaDeletedCodeMap[event.ContractAddr]; !ok {
			idaDeletedCodeMap[event.ContractAddr] = make([]string, 0)
		}
		idaDeletedCodeMap[event.ContractAddr] = append(idaDeletedCodeMap[event.ContractAddr], idaDeleteIds...)
		idaEventResult.EventTime = event.Timestamp
	}
}

// DealTopicEventData 解析eventDate
func DealTopicEventData(contractEvent []*db.ContractEvent, contractInfoMap map[string]*db.Contract,
	txInfoMap map[string]*db.Transaction) *model.TopicEventResult {
	ownerAddrMap := make(map[string]string, 0)
	didAccountMap := make(map[string][]string, 0)
	bnsAccountMap := make(map[string]*db.BNSTopicEventData, 0)
	idaEventResult := newIDAEventResult()
	topicEventResult := newTopicEventResult()

	if len(contractEvent) == 0 {
		return topicEventResult
	}

	for _, event := range contractEvent {
		eventData := parseEventData(event)
		if len(eventData) == 0 {
			continue
		}

		//解析IDA数据
		processIDAEvent(event, eventData, idaEventResult)
		//解析BNS事件，BNS绑定，解绑
		processBNSEvent(event, eventData, bnsAccountMap, topicEventResult)
		//解析DID事件，设置DID
		processDIDEvent(event, eventData, didAccountMap, topicEventResult)
		//解析黑名单交易，添加，删除黑名单
		processBlackListEvent(topicEventResult, event.ContractName, event.Topic, eventData)

		//合约信息
		contract := contractInfoMap[event.ContractName]
		if contract == nil {
			continue
		}

		//解析身份认证合约
		BuildIdentityEventData(topicEventResult, contractInfoMap, event, eventData)

		//流转数据解析
		var senderUser string
		if txInfo, ok := txInfoMap[event.TxId]; ok {
			senderUser = txInfo.UserAddr
		}
		//根据eventData解析transfer流转记录
		ownerAddrMap = BuildTransferEventData(topicEventResult, ownerAddrMap, contractInfoMap, event,
			senderUser, eventData)
	}

	//持仓地址列表
	ownerAdders := make([]string, 0)
	for _, addr := range ownerAddrMap {
		ownerAdders = append(ownerAdders, addr)
	}
	topicEventResult.OwnerAdders = ownerAdders

	//DID列表
	if len(didAccountMap) > 0 {
		topicEventResult.DIDAccount = didAccountMap
	}

	for _, value := range bnsAccountMap {
		topicEventResult.BNSBindEventData = append(topicEventResult.BNSBindEventData, value)
	}

	//IDA数据
	topicEventResult.IDAEventData = idaEventResult
	return topicEventResult
}

// 解析BNS事件，BNS绑定，解绑
func processBNSEvent(event *db.ContractEvent, eventData []string, bnsAccountMap map[string]*db.BNSTopicEventData,
	topicEventResult *model.TopicEventResult) {
	bnsBindEventData, bnsUnBindDomain := DealUserBNSEventData(event.ContractName, event.Topic, eventData)
	if bnsBindEventData != nil {
		bnsAccountMap[bnsBindEventData.Domain] = bnsBindEventData
	}
	//bns解绑
	if bnsUnBindDomain != "" {
		//如果前面有绑定，需要删除
		delete(bnsAccountMap, bnsUnBindDomain)
		topicEventResult.BNSUnBindDomain = append(topicEventResult.BNSUnBindDomain, bnsUnBindDomain)
	}
}

// processBlackListEvent 构造交易黑名单数据
func processBlackListEvent(topicEventResult *model.TopicEventResult, contractName, topic string, eventData []string) {
	//解析黑名单交易，添加，删除黑名单
	addBlack, deleteBlack := DealBackListEventData(contractName, topic, eventData)
	if len(addBlack) > 0 {
		topicEventResult.AddBlack = append(topicEventResult.AddBlack, addBlack...)
	}
	if len(deleteBlack) > 0 {
		topicEventResult.DeleteBlack = append(topicEventResult.DeleteBlack, deleteBlack...)
	}
}

// 解析DID事件，设置DID
func processDIDEvent(event *db.ContractEvent, eventData []string, didAccountMap map[string][]string,
	topicEventResult *model.TopicEventResult) {
	did, didAddrs := BuildDIDEventData(event.ContractName, event.Topic, eventData)
	if did != "" {
		//如果重复绑定，会覆盖didAccountMap的数据，以最后一次绑定为准
		didAccountMap[did] = didAddrs
		//DID解绑列表，
		//所有涉及到的DID都先解绑，在重新绑定。
		topicEventResult.DIDUnBindList = append(topicEventResult.DIDUnBindList, did)
	}
}

// BuildTransferEventData 构造流转数据
func BuildTransferEventData(topicEventResult *model.TopicEventResult, ownerAddrMap map[string]string,
	contractInfoMap map[string]*db.Contract, event *db.ContractEvent, senderUser string,
	eventData []string) map[string]string {
	//合约信息
	contract := contractInfoMap[event.ContractName]
	if contract == nil {
		return ownerAddrMap
	}

	//根据eventData解析transfer流转记录
	topicEventData := GetTransferEventDta(contract.ContractType, event.Topic, senderUser, eventData)
	if topicEventData == nil {
		return ownerAddrMap
	}

	if topicEventData.TokenId != "" || topicEventData.Amount != "" {
		//统计持仓地址
		if topicEventData.FromAddress != "" {
			if _, ok := ownerAddrMap[topicEventData.FromAddress]; !ok {
				ownerAddrMap[topicEventData.FromAddress] = topicEventData.FromAddress
			}
		}
		if topicEventData.ToAddress != "" {
			if _, ok := ownerAddrMap[topicEventData.ToAddress]; !ok {
				ownerAddrMap[topicEventData.ToAddress] = topicEventData.ToAddress
			}
		}
	}

	transferData := &db.ContractEventData{
		Topic:        event.Topic,
		Index:        event.EventIndex,
		TxId:         event.TxId,
		ContractName: event.ContractName,
		EventData:    topicEventData,
		Timestamp:    event.Timestamp,
	}
	topicEventResult.ContractEventData = append(topicEventResult.ContractEventData, transferData)
	return ownerAddrMap
}

// BuildIdentityEventData 解析身份认证合约
func BuildIdentityEventData(topicEventResult *model.TopicEventResult, contractInfoMap map[string]*db.Contract,
	event *db.ContractEvent, eventData []string) {
	//合约信息
	contract := contractInfoMap[event.ContractName]
	if contract == nil || contract.ContractType != common.ContractStandardNameCMID {
		return
	}

	//解析身份认证合约
	identityEventData := DealIdentityEventData(event.Topic, eventData)
	if identityEventData == nil {
		return
	}

	newUUID := uuid.New().String()
	tempIdentity := &db.IdentityContract{
		ID:           newUUID,
		TxId:         event.TxId,
		EventIndex:   event.EventIndex,
		ContractName: contract.Name,
		ContractAddr: contract.Addr,
		UserAddr:     identityEventData.UserAddr,
		Level:        identityEventData.Level,
		PkPem:        identityEventData.PkPem,
	}
	topicEventResult.IdentityContract = append(topicEventResult.IdentityContract, tempIdentity)
}

// BuildBNSEventData 构造BNS数据
func BuildBNSEventData(topicEventResult *model.TopicEventResult, contractName, topic string, eventData []string) {
	//解析BNS事件，BNS绑定，解绑
	bnsBindEventData, bnsUnBindDomain := DealUserBNSEventData(contractName, topic, eventData)
	if bnsBindEventData != nil {
		//BNS绑定事件
		topicEventResult.BNSBindEventData = append(topicEventResult.BNSBindEventData, bnsBindEventData)
	}
	if bnsUnBindDomain != "" {
		//BNS解绑事件
		topicEventResult.BNSUnBindDomain = append(topicEventResult.BNSUnBindDomain, bnsUnBindDomain)
	}
}

// BuildDIDEventData 构造DID数据
func BuildDIDEventData(contractName, topic string, eventData []string) (string, []string) {
	var accountAddrs []string
	didDocument := DealUserDIDEventData(contractName, topic, eventData)
	if didDocument == nil {
		return "", accountAddrs
	}

	//一个DID绑定的所有账户
	for _, value := range didDocument.VerificationMethod {
		accountAddrs = append(accountAddrs, value.Address)
	}

	return didDocument.Did, accountAddrs
}

// UpdateContractTxAndEventNum 更新合约交易数和事件数
func UpdateContractTxAndEventNum(minHeight int64, contractMap map[string]*db.Contract,
	txList map[string]*db.Transaction, contractEvent []*db.ContractEvent) []*db.Contract {
	contractTxNumMap := make(map[string]int64, 0)
	contractEventNumMap := make(map[string]int64, 0)
	updateContractMap := make(map[string]*db.Contract, 0)
	updateContractNum := make([]*db.Contract, 0)
	if len(contractMap) == 0 || len(txList) == 0 {
		return updateContractNum
	}

	//统计本次交易数据量
	for _, txInfo := range txList {
		if contract, ok := contractMap[txInfo.ContractNameBak]; ok {
			//说明已经更新过了
			if contract.BlockHeight >= minHeight {
				continue
			}
			contractTxNumMap[contract.Addr]++
		}
	}

	//统计本次合约事件数据量
	for _, event := range contractEvent {
		if contract, ok := contractMap[event.ContractNameBak]; ok {
			//说明已经更新过了
			if contract.BlockHeight >= minHeight {
				continue
			}
			contractEventNumMap[contract.Addr]++
		}
	}

	for addr, txNum := range contractTxNumMap {
		var contractInfo *db.Contract
		if contract, ok := updateContractMap[addr]; ok {
			contractInfo = contract
		} else if contract, ok = contractMap[addr]; ok {
			contractInfo = contract
		} else {
			continue
		}
		contractInfo.TxNum = contractInfo.TxNum + txNum
		contractInfo.BlockHeight = minHeight
		updateContractMap[addr] = contractInfo
	}

	for addr, eventNum := range contractEventNumMap {
		var contractInfo *db.Contract
		if contract, ok := updateContractMap[addr]; ok {
			contractInfo = contract
		} else if contract, ok = contractMap[addr]; ok {
			contractInfo = contract
		} else {
			continue
		}
		contractInfo.EventNum = contractInfo.EventNum + eventNum
		contractInfo.BlockHeight = minHeight
		updateContractMap[addr] = contractInfo
	}

	for _, contract := range updateContractMap {
		updateContractNum = append(updateContractNum, contract)
	}

	return updateContractNum
}

// DealEvidence 处理存证合约
// @param blockHeight 区块高度
// @param txInfo 交易信息
// @param userInfo 用户信息
// @return evidences 存证信息
// @return err 错误信息
func DealEvidence(blockHeight int64, txInfo *pbCommon.Transaction, userInfo *db.SenderPayerUser) (
	evidences []*db.EvidenceContract, err error) {
	evidences = make([]*db.EvidenceContract, 0)
	//判断是否是存证合约，如果是存证合约，则解析存证信息
	if txInfo.Payload.Method != common.PayloadMethodEvidence &&
		txInfo.Payload.Method != common.PayloadMethodEvidenceBatch {
		return evidences, nil
	}

	contractName := txInfo.Payload.ContractName
	tempEvidence := &db.EvidenceContract{
		ContractName:       contractName,
		TxId:               txInfo.Payload.TxId,
		SenderAddr:         userInfo.SenderUserAddr,
		Timestamp:          txInfo.Payload.Timestamp,
		BlockHeight:        blockHeight,
		ContractResult:     txInfo.Result.ContractResult.Result,
		ContractResultCode: txInfo.Result.ContractResult.Code,
	}
	//判断是单条存证还是批量存证，如果是单条存证，则解析单条存证信息，如果是批量存证，则解析批量存证信息
	if txInfo.Payload.Method == common.PayloadMethodEvidence {
		//单条存证
		for _, parameter := range txInfo.Payload.Parameters {
			if parameter.Key == "hash" {
				tempEvidence.Hash = string(parameter.Value)
			}
			if parameter.Key == "metadata" {
				tempEvidence.MetaData = string(parameter.Value)
			}
			if parameter.Key == "id" {
				tempEvidence.EvidenceId = string(parameter.Value)
			}
		}
		newUUID := uuid.New().String()
		tempEvidence.ID = newUUID
		evidences = append(evidences, tempEvidence)
	} else if txInfo.Payload.Method == common.PayloadMethodEvidenceBatch {
		//批量存证
		for _, parameter := range txInfo.Payload.Parameters {
			if parameter.Key == "evidences" {
				standardEvidences := make([]standard.Evidence, 0)
				err = json.Unmarshal(parameter.Value, &standardEvidences)
				if err != nil {
					return evidences, err
				}
				for _, e := range standardEvidences {
					newUUID := uuid.New().String()
					tempEvidence.EvidenceId = e.Id
					tempEvidence.Hash = e.Hash
					tempEvidence.MetaData = e.Metadata
					tempEvidence.ID = newUUID
					evidences = append(evidences, tempEvidence)
				}
			}
		}
	}
	return evidences, err
}

// BuildIDAEventData 解析IDA数据
// @param contractType 合约类型
// @param topic 事件类型
// @param eventData 事件数据
// @return idaInfoList 解析后的IDA信息
// @return idaUpdateData 解析后的IDA更新信息
// @return idaIds 解析后的IDA删除信息
func BuildIDAEventData(contractType, topic string, eventData []string) (
	[]*standard.IDAInfo, *db.EventIDAUpdatedData, []string) {
	idaIds := make([]string, 0)
	idaInfoList := make([]*standard.IDAInfo, 0)
	var idaUpdateData *db.EventIDAUpdatedData
	//判断是否是IDA合约
	if contractType != standard.ContractStandardNameCMIDA {
		return idaInfoList, idaUpdateData, idaIds
	}

	switch topic {
	case standard.EventIDACreated:
		idaInfoList = DealEventIDACreated(eventData)
	case standard.EventIDAUpdated:
		idaUpdateData = DealEventIDAUpdated(eventData)
	case standard.EventIDADeleted:
		idaIds = DealEventIDADeleted(eventData)
	}

	return idaInfoList, idaUpdateData, idaIds
}

// DealEventTopicTxNum 统计每个Topic在当前区块中出现的次数
// @param contractEvents 合约事件
// @return idaInfoList 解析后的IDA信息
func DealEventTopicTxNum(contractEvents []*db.ContractEvent) map[string]map[string]int64 {
	// 创建一个嵌套映射，外层映射以 ContractName 为键，内层映射以 Topic 为键
	counts := make(map[string]map[string]int64)

	for _, event := range contractEvents {
		// 初始化内层映射
		if counts[event.ContractName] == nil {
			counts[event.ContractName] = make(map[string]int64)
		}
		// 统计 Topic 的出现次数
		counts[event.ContractName][event.Topic]++
	}
	return counts
}

// ProcessEventTopicTxNum 统计每个Topic在当前区块中出现的次数
// @param eventTopicTxNum 每个Topic在当前区块中出现的次数
// @param eventTopicDBMap 每个Topic在数据库中的最新数据
// @param blockHeight 当前区块高度
// @return insertEventTpic 需要插入的Topic数据
// ProcessEventTopicTxNum 函数用于处理事件主题的交易数量
func ProcessEventTopicTxNum(eventTopicTxNum map[string]map[string]int64,
	eventTopicDBMap map[string]map[string]*db.ContractEventTopic, blockHeight int64) (
	[]*db.ContractEventTopic, []*db.ContractEventTopic) {
	// 定义插入和更新事件主题的切片
	insertEventTpic := make([]*db.ContractEventTopic, 0)
	updateEventTpic := make([]*db.ContractEventTopic, 0)
	// 遍历事件主题的交易数量
	for contractName, topics := range eventTopicTxNum {
		for topic, num := range topics {
			// 如果事件主题在数据库中存在
			if eventMapDB, exists := eventTopicDBMap[contractName]; exists {
				if eventDB, ok := eventMapDB[topic]; ok {
					// 如果当前区块高度小于等于数据库中的区块高度，则跳过
					if blockHeight <= eventDB.BlockHeight {
						continue
					}

					// 更新交易数量
					txNum := eventDB.TxNum + num
					updateEventTpic = append(updateEventTpic, &db.ContractEventTopic{
						Topic:        topic,
						ContractName: contractName,
						BlockHeight:  blockHeight,
						TxNum:        txNum,
					})
					continue
				}
			}

			// 生成新的UUID
			newUUID := uuid.New().String()
			// 插入新的事件主题
			insert := &db.ContractEventTopic{
				ID:           newUUID,
				Topic:        topic,
				ContractName: contractName,
				TxNum:        num,
				BlockHeight:  blockHeight,
			}
			insertEventTpic = append(insertEventTpic, insert)
		}
	}
	// 返回插入和更新的事件主题
	return insertEventTpic, updateEventTpic
}
