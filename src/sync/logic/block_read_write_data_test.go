package logic

import (
	"chainmaker_web/src/config"
	"chainmaker_web/src/db"
	"chainmaker_web/src/sync/common"
	"chainmaker_web/src/sync/model"
	"encoding/json"
	"fmt"
	"os"
	"reflect"
	"sync"
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"

	"chainmaker.org/chainmaker/contract-utils/standard"
	pbCommon "chainmaker.org/chainmaker/pb-go/v2/common"
	pbConfig "chainmaker.org/chainmaker/pb-go/v2/config"
	tcipCommon "chainmaker.org/chainmaker/tcip-go/v2/common"
)

type JsonData struct {
	UpdatePositionData map[string]*db.PositionData `json:"UpdatePositionData"`
	ResultPositionData ResultPositionData          `json:"ResultPositionData"`
}

type ResultPositionData struct {
	FungiblePosition    []*db.FungiblePosition    `json:"FungiblePosition"`
	NonFungiblePosition []*db.NonFungiblePosition `json:"NonFungiblePosition"`
}

func geFileData(fileName string) (*json.Decoder, error) {
	file, err := os.Open("../testData/" + fileName)
	if err != nil {
		fmt.Println("Error opening file:", err)
		return nil, err
	}

	// 解码 JSON 文件内容到 blockInfo 结构体
	decoder := json.NewDecoder(file)
	return decoder, err
}

func getUpdatePositionDataTest(fileName string) (map[string]*db.PositionData, []*db.FungiblePosition,
	[]*db.NonFungiblePosition) {
	//positionData := make(map[string]*db.PositionData, 0)
	//fungiblePosition := make([]*db.FungiblePosition, 0)
	//nonFungiblePosition := make([]*db.NonFungiblePosition, 0)
	// 打开 JSON 文件
	decoder, err := geFileData(fileName)
	if err != nil {
		fmt.Println("Error opening file:", err)
		return nil, nil, nil
	}

	// 解码 JSON 文件内容到 blockInfo 结构体
	var jsonData JsonData
	err = decoder.Decode(&jsonData)
	if err != nil {
		fmt.Println("Error decoding JSON:", err)
		return nil, nil, nil
	}
	return jsonData.UpdatePositionData, jsonData.ResultPositionData.FungiblePosition,
		jsonData.ResultPositionData.NonFungiblePosition
}

func getPositionDBJsonTest(fileName string) map[string][]*db.FungiblePosition {
	resultValue := make(map[string][]*db.FungiblePosition, 0)
	// 打开 JSON 文件
	decoder, err := geFileData(fileName)
	if err != nil {
		fmt.Println("Error opening file:", err)
		return resultValue
	}

	// 解码 JSON 文件内容到 blockInfo 结构体
	err = decoder.Decode(&resultValue)
	if err != nil {
		fmt.Println("Error decoding JSON:", err)
		return resultValue
	}
	return resultValue
}

func getNonPositionDBJsonTest(fileName string) map[string][]*db.NonFungiblePosition {
	resultValue := make(map[string][]*db.NonFungiblePosition, 0)
	// 打开 JSON 文件
	decoder, err := geFileData(fileName)
	if err != nil {
		fmt.Println("Error opening file:", err)
		return resultValue
	}
	err = decoder.Decode(&resultValue)
	if err != nil {
		fmt.Println("Error decoding JSON:", err)
		return resultValue
	}
	return resultValue
}

func getGotTokenResultTest(fileName string) *db.TokenResult {
	resultValue := &db.TokenResult{}
	// 打开 JSON 文件
	decoder, err := geFileData(fileName)
	if err != nil {
		fmt.Println("Error opening file:", err)
		return resultValue
	}
	err = decoder.Decode(&resultValue)
	if err != nil {
		fmt.Println("Error decoding JSON:", err)
		return resultValue
	}
	return resultValue
}

func getChainListConfigTest(fileName string) []*config.ChainInfo {
	resultValue := make([]*config.ChainInfo, 0)
	// 打开 JSON 文件
	decoder, err := geFileData(fileName)
	if err != nil {
		fmt.Println("Error opening file:", err)
		return resultValue
	}
	err = decoder.Decode(&resultValue)
	if err != nil {
		fmt.Println("Error decoding JSON:", err)
		return resultValue
	}
	return resultValue
}

func getChainTxRWSetTest(fileName string) *pbCommon.TxRWSet {
	var resultValue *pbCommon.TxRWSet
	// 打开 JSON 文件
	decoder, err := geFileData(fileName)
	if err != nil {
		fmt.Println("Error opening file:", err)
		return resultValue
	}
	err = decoder.Decode(&resultValue)
	if err != nil {
		fmt.Println("Error decoding JSON:", err)
		return resultValue
	}
	return resultValue
}

func getBlockInfoTest(fileName string) *pbCommon.BlockInfo {
	var blockInfo *pbCommon.BlockInfo
	// 打开 JSON 文件
	//file, err := os.Open("../testData/1_blockInfoJsonContract.json")

	decoder, err := geFileData(fileName)
	if err != nil {
		fmt.Println("Error opening file:", err)
		return blockInfo
	}

	// 解码 JSON 文件内容到 blockInfo 结构体
	err = decoder.Decode(&blockInfo)
	if err != nil {
		fmt.Println("Error decoding JSON:", err)
		return blockInfo
	}
	return blockInfo
}

func getDealResultTest(fileName string) *model.ProcessedBlockData {
	var dealResult *model.ProcessedBlockData
	// 打开 JSON 文件
	decoder, err := geFileData(fileName)
	if err != nil {
		fmt.Println("Error opening file:", err)
		return dealResult
	}
	// 解码 JSON 文件内容到 blockInfo 结构体
	err = decoder.Decode(&dealResult)
	if err != nil {
		fmt.Println("Error decoding JSON:", err)
		return dealResult
	}
	return dealResult
}

func getTxInfoInfoTest(fileName string) *pbCommon.Transaction {
	var txInfo *pbCommon.Transaction
	// 打开 JSON 文件
	decoder, err := geFileData(fileName)
	if err != nil {
		fmt.Println("Error opening file:", err)
		return txInfo
	}
	// 解码 JSON 文件内容到 blockInfo 结构体
	err = decoder.Decode(&txInfo)
	if err != nil {
		fmt.Println("Error decoding JSON:", err)
		return txInfo
	}
	return txInfo
}

func getBuildTxInfoTest(fileName string) *db.Transaction {
	var txInfo *db.Transaction
	// 打开 JSON 文件
	decoder, err := geFileData(fileName)
	if err != nil {
		fmt.Println("Error opening file:", err)
		return txInfo
	}
	// 解码 JSON 文件内容到 blockInfo 结构体
	err = decoder.Decode(&txInfo)
	if err != nil {
		fmt.Println("Error decoding JSON:", err)
		return txInfo
	}
	return txInfo
}

func getUserInfoInfoTest(fileName string) *common.MemberAddrIdCert {
	var userInfo *common.MemberAddrIdCert
	// 打开 JSON 文件
	decoder, err := geFileData(fileName)
	if err != nil {
		fmt.Println("Error opening file:", err)
		return userInfo
	}
	// 解码 JSON 文件内容到 blockInfo 结构体
	err = decoder.Decode(&userInfo)
	if err != nil {
		fmt.Println("Error decoding JSON:", err)
		return userInfo
	}
	return userInfo
}

func getCrossChainInfoTest(fileName string) *tcipCommon.CrossChainInfo {
	var resultValue *tcipCommon.CrossChainInfo
	// 打开 JSON 文件
	decoder, err := geFileData(fileName)
	if err != nil {
		fmt.Println("Error opening file:", err)
		return resultValue
	}
	// 解码 JSON 文件内容到 blockInfo 结构体
	err = decoder.Decode(&resultValue)
	if err != nil {
		fmt.Println("Error decoding JSON:", err)
		return resultValue
	}
	return resultValue
}

func getContractEventTest(fileName string) []*db.ContractEventData {
	contractEvents := make([]*db.ContractEventData, 0)
	// 打开 JSON 文件
	//file, err := os.Open("../testData/1_blockInfoJsonContract.json")
	decoder, err := geFileData(fileName)
	if err != nil {
		fmt.Println("Error opening file:", err)
		return contractEvents
	}
	err = decoder.Decode(&contractEvents)
	if err != nil {
		fmt.Println("Error decoding JSON:", err)
		return contractEvents
	}
	return contractEvents
}

func getContractInfoMapTest(fileName string) map[string]*db.Contract {
	resultValue := make(map[string]*db.Contract, 0)
	// 打开 JSON 文件
	decoder, err := geFileData(fileName)
	if err != nil {
		fmt.Println("Error opening file:", err)
		return resultValue
	}

	err = decoder.Decode(&resultValue)
	if err != nil {
		fmt.Println("Error decoding JSON:", err)
		return resultValue
	}
	return resultValue
}

func getAccountMapTest(fileName string) map[string]*db.Account {
	resultValue := make(map[string]*db.Account, 0)
	// 打开 JSON 文件
	decoder, err := geFileData(fileName)
	if err != nil {
		fmt.Println("Error opening file:", err)
		return resultValue
	}
	err = decoder.Decode(&resultValue)
	if err != nil {
		fmt.Println("Error decoding JSON:", err)
		return resultValue
	}
	return resultValue
}

func getPositionListJsonTest(fileName string) map[string]*db.PositionData {
	resultValue := make(map[string]*db.PositionData, 0)
	// 打开 JSON 文件
	decoder, err := geFileData(fileName)
	if err != nil {
		fmt.Println("Error opening file:", err)
		return resultValue
	}
	err = decoder.Decode(&resultValue)
	if err != nil {
		fmt.Println("Error decoding JSON:", err)
		return resultValue
	}
	return resultValue
}

func getIdaAssetTest(fileName string) *standard.IDAInfo {
	var idaInfo *standard.IDAInfo
	// 打开 JSON 文件
	decoder, err := geFileData(fileName)
	if err != nil {
		fmt.Println("Error opening file:", err)
		return idaInfo
	}
	// 解码 JSON 文件内容到 blockInfo 结构体
	err = decoder.Decode(&idaInfo)
	if err != nil {
		fmt.Println("Error decoding JSON:", err)
		return idaInfo
	}
	return idaInfo
}

func TestGetChainConfigByWriteSet(t *testing.T) {
	type args struct {
		txRWSet *pbCommon.TxRWSet
		txInfo  *pbCommon.Transaction
	}
	txInfo := getTxInfoInfoTest("1_txInfoJsonContract.json")
	tests := []struct {
		name    string
		args    args
		want    *pbConfig.ChainConfig
		wantErr bool
	}{
		{
			name: "test case 1",
			args: args{
				txRWSet: nil,
				txInfo:  txInfo,
			},
			want:    nil,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := GetChainConfigByWriteSet(tt.args.txRWSet, tt.args.txInfo)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetChainConfigByWriteSet() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetChainConfigByWriteSet() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetCrossChainInfoByWriteSet(t *testing.T) {
	txInfo := getTxInfoInfoTest("cross_1_txInfoJson.json")
	txRWSet := getChainTxRWSetTest("cross_1_txRWSet.json")
	crossChainInfo := getCrossChainInfoTest("cross_1_ChainInfoJson.json")

	type args struct {
		txRWSet *pbCommon.TxRWSet
		txInfo  *pbCommon.Transaction
	}
	tests := []struct {
		name    string
		args    args
		want    *tcipCommon.CrossChainInfo
		wantErr bool
	}{
		{
			name: "test case 1",
			args: args{
				txRWSet: txRWSet,
				txInfo:  txInfo,
			},
			want:    crossChainInfo,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := GetCrossChainInfoByWriteSet(tt.args.txRWSet, tt.args.txInfo)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetCrossChainInfoByWriteSet() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetCrossChainInfoByWriteSet() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetSubChainBlockHeightByWriteSet(t *testing.T) {
	txInfo := getTxInfoInfoTest("cross_2_txInfoJson_1709178475.json")
	txRWSet := getChainTxRWSetTest("cross_2_txRWSetJson_1709178475.json")
	type args struct {
		txRWSet *pbCommon.TxRWSet
		txInfo  *pbCommon.Transaction
	}
	tests := []struct {
		name  string
		args  args
		want  int64
		want1 string
	}{
		{
			name: "test case 1",
			args: args{
				txRWSet: txRWSet,
				txInfo:  txInfo,
			},
			want:  14,
			want1: "official_spv0chainmaker001",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1 := GetSubChainBlockHeightByWriteSet(tt.args.txRWSet, tt.args.txInfo)
			if got != tt.want {
				t.Errorf("GetSubChainBlockHeightByWriteSet() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("GetSubChainBlockHeightByWriteSet() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func TestGetSubChainGatewayIdByWriteSet(t *testing.T) {
	txInfo := getTxInfoInfoTest("cross_3_txInfoJson_1709189120.json")
	txRWSet := getChainTxRWSetTest("cross_3_txRWSetJson_1709189120.json")
	type args struct {
		txRWSet *pbCommon.TxRWSet
		txInfo  *pbCommon.Transaction
	}
	tests := []struct {
		name string
		args args
		want int64
	}{
		{
			name: "test case 1",
			args: args{
				txRWSet: txRWSet,
				txInfo:  txInfo,
			},
			want: 3,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := GetSubChainGatewayIdByWriteSet(tt.args.txRWSet, tt.args.txInfo)
			if got == nil || !reflect.DeepEqual(*got, tt.want) {
				t.Errorf("GetSubChainGatewayIdByWriteSet() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_processCrossChainInfo(t *testing.T) {
	txInfo := getTxInfoInfoTest("cross_1_txInfoJson.json")
	crossChainInfo := getCrossChainInfoTest("cross_1_ChainInfoJson.json")

	var (
		wantGot  *db.CrossMainTransaction
		wantGot1 []*db.CrossTransactionTransfer
		wantGot2 = make(map[string]*db.CrossBusinessTransaction, 0)
	)
	wantGotJson := "{\"txId\":\"AC1052E93C62A79B5ECF942AC1BF37ED8CD9F9593873DBC81807DF8CBE5E451F\",\"crossId\":\"0\",\"chainMsg\":\"[{\\\"gateway_id\\\":\\\"0\\\",\\\"chain_rid\\\":\\\"chainmaker001\\\",\\\"contract_name\\\":\\\"crossChainSaveQuery\\\",\\\"method\\\":\\\"save\\\",\\\"parameter\\\":\\\"{\\\\\\\"key\\\\\\\":\\\\\\\"main_son_save_key\\\\\\\",\\\\\\\"value\\\\\\\":\\\\\\\"main_son_save_value\\\\\\\"}\\\",\\\"confirm_info\\\":{\\\"chain_rid\\\":\\\"chainmaker001\\\",\\\"contract_name\\\":\\\"crossChainSaveQuery\\\",\\\"method\\\":\\\"saveState\\\",\\\"parameter\\\":\\\"{\\\\\\\"key\\\\\\\":\\\\\\\"main_son_save_key\\\\\\\",\\\\\\\"state\\\\\\\":\\\\\\\"confirmEnd\\\\\\\"}\\\"},\\\"cancel_info\\\":{\\\"chain_rid\\\":\\\"chainmaker001\\\",\\\"contract_name\\\":\\\"crossChainSaveQuery\\\",\\\"method\\\":\\\"saveState\\\",\\\"parameter\\\":\\\"{\\\\\\\"key\\\\\\\":\\\\\\\"main_son_save_key\\\\\\\",\\\\\\\"state\\\\\\\":\\\\\\\"cancelEnd\\\\\\\"}\\\"}}]\",\"status\":1,\"crossType\":1,\"timestamp\":1709177844,\"ID\":0,\"CreatedAt\":\"0001-01-01T00:00:00Z\",\"UpdatedAt\":\"0001-01-01T00:00:00Z\",\"DeletedAt\":null}"
	wantGot1Json := "[{\"crossId\":\"0\",\"fromGatewayId\":\"MAIN_GATEWAY_ID-relay-1\",\"fromChainId\":\"testChainId\",\"fromIsMainChain\":true,\"toGatewayId\":\"0\",\"toChainId\":\"chainmaker001\",\"toIsMainChain\":false,\"blockHeight\":12,\"contractName\":\"crossChainSaveQuery\",\"contractMethod\":\"save\",\"parameter\":\"{\\\"key\\\":\\\"main_son_save_key\\\",\\\"value\\\":\\\"main_son_save_value\\\"}\",\"ID\":0,\"CreatedAt\":\"0001-01-01T00:00:00Z\",\"UpdatedAt\":\"0001-01-01T00:00:00Z\",\"DeletedAt\":null}]"
	_ = json.Unmarshal([]byte(wantGotJson), &wantGot)
	_ = json.Unmarshal([]byte(wantGot1Json), &wantGot1)

	type args struct {
		chainId        string
		blockHeight    int64
		crossChainInfo *tcipCommon.CrossChainInfo
		txInfo         *pbCommon.Transaction
	}
	tests := []struct {
		name  string
		args  args
		want  *db.CrossMainTransaction
		want1 []*db.CrossTransactionTransfer
		want2 map[string]*db.CrossBusinessTransaction
	}{
		{
			name: "test case 1",
			args: args{
				chainId:        ChainId,
				blockHeight:    12,
				crossChainInfo: crossChainInfo,
				txInfo:         txInfo,
			},
			want:  wantGot,
			want1: wantGot1,
			want2: wantGot2,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1, got2, _, _, _ := processCrossChainInfo(tt.args.chainId, tt.args.blockHeight, tt.args.crossChainInfo, tt.args.txInfo)
			if !cmp.Equal(got, tt.want, cmpopts.IgnoreFields(db.CrossMainTransaction{}, "CreatedAt", "UpdatedAt")) {
				t.Errorf("processCrossChainInfo() got = %v, want %v\ndiff: %s", got, tt.want, cmp.Diff(got, tt.want))
			}

			if !cmp.Equal(got1, tt.want1, cmpopts.IgnoreFields(db.CrossTransactionTransfer{}, "CreatedAt", "UpdatedAt", "BlockHeight", "ID")) {
				t.Errorf("processCrossChainInfo() got1 = %v, want1 %v\ndiff: %s", got1, tt.want1, cmp.Diff(got1, tt.want1))
			}

			if !reflect.DeepEqual(got2, tt.want2) {
				t.Errorf("processCrossChainInfo() got2 = %v, want %v", got2, tt.want2)
			}
		})
	}
}

func TestParallelParseWriteSetData(t *testing.T) {
	blockInfo := getBlockInfoTest("2_blockInfoJson_1704945589.json")
	if blockInfo == nil || len(blockInfo.RwsetList) == 0 {
		return
	}

	// Test case 1: Normal case with multiple transactions
	dealResult1 := &model.ProcessedBlockData{}
	err1 := ParallelParseWriteSetData(blockInfo, dealResult1)
	if err1 != nil {
		t.Errorf("Test case 1 failed: %v", err1)
	}

	// Test case 2: Empty block
	blockInfo2 := &pbCommon.BlockInfo{
		Block: &pbCommon.Block{
			Header: &pbCommon.BlockHeader{
				ChainId:     ChainId,
				BlockHeight: 1,
			},
			Txs: []*pbCommon.Transaction{},
		},
	}
	dealResult2 := &model.ProcessedBlockData{}
	err2 := ParallelParseWriteSetData(blockInfo2, dealResult2)
	if err2 != nil {
		t.Errorf("Test case 2 failed: %v", err2)
	}

	// Test case 3: Block with only one transaction
	blockInfo3 := &pbCommon.BlockInfo{
		Block: &pbCommon.Block{
			Header: &pbCommon.BlockHeader{
				ChainId:     ChainId,
				BlockHeight: 1,
			},
			Txs: []*pbCommon.Transaction{
				{
					Payload: &pbCommon.Payload{
						TxType:    pbCommon.TxType_INVOKE_CONTRACT,
						Timestamp: 1625097700,
					},
				},
			},
		},
	}
	dealResult3 := &model.ProcessedBlockData{}
	err3 := ParallelParseWriteSetData(blockInfo3, dealResult3)
	if err3 != nil {
		t.Errorf("Test case 3 failed: %v", err3)
	}
}

func TestProcessWriteSetDataOther(t *testing.T) {
	var mutx sync.Mutex
	// Test case 1: Normal case with valid write set
	rwSetList1 := &pbCommon.TxRWSet{
		TxWrites: []*pbCommon.TxWrite{
			{
				Key:   []byte("chain1"),
				Value: []byte("value1"),
			},
		},
	}
	txInfo1 := &pbCommon.Transaction{
		Payload: &pbCommon.Payload{
			TxType:    pbCommon.TxType_INVOKE_CONTRACT,
			Timestamp: 1625097700,
		},
	}
	dealResult1 := &model.ProcessedBlockData{}
	err1 := processWriteSetDataOther(&mutx, rwSetList1, txInfo1, dealResult1)
	if err1 != nil {
		t.Errorf("Test case 1 failed: %v", err1)
	}

	// Test case 2: Empty write set
	rwSetList2 := &pbCommon.TxRWSet{
		TxWrites: []*pbCommon.TxWrite{},
	}
	txInfo2 := &pbCommon.Transaction{
		Payload: &pbCommon.Payload{
			TxType:    pbCommon.TxType_INVOKE_CONTRACT,
			Timestamp: 1625097700,
		},
	}
	dealResult2 := &model.ProcessedBlockData{}
	err2 := processWriteSetDataOther(&mutx, rwSetList2, txInfo2, dealResult2)
	if err2 != nil {
		t.Errorf("Test case 2 failed: %v", err2)
	}
}

func TestProcessCrossChainTransaction(t *testing.T) {
	var mutx sync.Mutex
	// Test case 1: Normal case with valid write set
	rwSetList1 := &pbCommon.TxRWSet{
		TxWrites: []*pbCommon.TxWrite{
			{
				Key:   []byte("cross1"),
				Value: []byte("value1"),
			},
		},
	}
	txInfo1 := &pbCommon.Transaction{
		Payload: &pbCommon.Payload{
			TxType:    pbCommon.TxType_INVOKE_CONTRACT,
			Timestamp: 1625097700,
		},
	}

	blockHeight1 := int64(1)
	crossResult1 := model.NewCrossChainResult()
	err1 := processCrossChainTransaction(&mutx, rwSetList1, txInfo1, ChainId, blockHeight1, crossResult1)
	if err1 != nil {
		t.Errorf("Test case 1 failed: %v", err1)
	}

	// Test case 2: Empty write set
	rwSetList2 := &pbCommon.TxRWSet{
		TxWrites: []*pbCommon.TxWrite{},
	}
	txInfo2 := &pbCommon.Transaction{
		Payload: &pbCommon.Payload{
			TxType:    pbCommon.TxType_INVOKE_CONTRACT,
			Timestamp: 1625097700,
		},
	}
	blockHeight2 := int64(1)
	crossResult2 := model.NewCrossChainResult()
	err2 := processCrossChainTransaction(&mutx, rwSetList2, txInfo2, ChainId, blockHeight2, crossResult2)
	if err2 != nil {
		t.Errorf("Test case 2 failed: %v", err2)
	}

	// Test case 3: Invalid write set
	rwSetList3 := &pbCommon.TxRWSet{
		TxWrites: []*pbCommon.TxWrite{
			{
				Key:   []byte("invalid"),
				Value: []byte("value1"),
			},
		},
	}
	txInfo3 := &pbCommon.Transaction{
		Payload: &pbCommon.Payload{
			TxType:    pbCommon.TxType_INVOKE_CONTRACT,
			Timestamp: 1625097700,
		},
	}

	blockHeight3 := int64(1)
	crossResult3 := model.NewCrossChainResult()
	err3 := processCrossChainTransaction(&mutx, rwSetList3, txInfo3, ChainId, blockHeight3, crossResult3)
	if err3 != nil {
		t.Errorf("Test case 3 failed: %v", err3)
	}
}

func TestUpdateCrossChainResult(t *testing.T) {
	// Test case 1: Normal case with valid data
	crossResult1 := model.NewCrossChainResult()
	gateWayId1 := int64(1)
	subBlockHeight1 := int64(100)
	spvContractName1 := "contract1"
	crossChainContracts1 := []*CrossContract{
		{
			SubChainId:   "subChain1",
			ContractName: "contract1",
		},
	}
	UpdateCrossChainResult(crossResult1, &gateWayId1, subBlockHeight1, spvContractName1, crossChainContracts1)
}

func TestUpdateCrossChainResultTx(t *testing.T) {
	// Test case 1: Normal case with valid data
	crossResult1 := model.NewCrossChainResult()
	mainTransaction1 := &db.CrossMainTransaction{}
	crossTxTransferList1 := []*db.CrossTransactionTransfer{
		{
			CrossId:     "cross1",
			FromChainId: "chain1",
			ToChainId:   "chain2",
		},
	}
	saveCycleTx1 := &db.CrossCycleTransaction{}
	updateCycleTx1 := &db.CrossCycleTransaction{}
	businessTxMap1 := map[string]*db.CrossBusinessTransaction{
		"tx1": {
			CrossId: "CrossId",
		},
	}
	UpdateCrossChainResultTx(crossResult1, mainTransaction1, crossTxTransferList1, saveCycleTx1, updateCycleTx1, businessTxMap1)

	// Test case 2: Empty data
	crossResult2 := model.NewCrossChainResult()
	mainTransaction2 := &db.CrossMainTransaction{}
	crossTxTransferList2 := []*db.CrossTransactionTransfer{}
	saveCycleTx2 := &db.CrossCycleTransaction{}
	updateCycleTx2 := &db.CrossCycleTransaction{}
	businessTxMap2 := map[string]*db.CrossBusinessTransaction{}
	UpdateCrossChainResultTx(crossResult2, mainTransaction2, crossTxTransferList2, saveCycleTx2, updateCycleTx2, businessTxMap2)

	// Test case 3: Invalid data
	crossResult3 := model.NewCrossChainResult()
	mainTransaction3 := &db.CrossMainTransaction{}
	crossTxTransferList3 := []*db.CrossTransactionTransfer{}
	saveCycleTx3 := &db.CrossCycleTransaction{}
	updateCycleTx3 := &db.CrossCycleTransaction{}
	businessTxMap3 := map[string]*db.CrossBusinessTransaction{}
	UpdateCrossChainResultTx(crossResult3, mainTransaction3, crossTxTransferList3, saveCycleTx3, updateCycleTx3, businessTxMap3)
}

func TestProcessCrossChainInfo(t *testing.T) {
	// Test case 1: Normal case with valid data
	blockHeight1 := int64(1)
	crossChainInfo1 := &tcipCommon.CrossChainInfo{
		CrossChainId: "cross1",
		CrossChainMsg: []*tcipCommon.CrossChainMsg{
			{
				ChainRid:     "subChain1",
				ContractName: "contract1",
			},
		},
	}
	txInfo1 := &pbCommon.Transaction{
		Payload: &pbCommon.Payload{
			TxType:    pbCommon.TxType_INVOKE_CONTRACT,
			Timestamp: 1625097700,
		},
	}
	processCrossChainInfo(ChainId, blockHeight1, crossChainInfo1, txInfo1)

	// Test case 2: Empty data
	blockHeight2 := int64(1)
	crossChainInfo2 := &tcipCommon.CrossChainInfo{}
	txInfo2 := &pbCommon.Transaction{
		Payload: &pbCommon.Payload{
			TxType:    pbCommon.TxType_INVOKE_CONTRACT,
			Timestamp: 1625097700,
		},
	}
	processCrossChainInfo(ChainId, blockHeight2, crossChainInfo2, txInfo2)
}
