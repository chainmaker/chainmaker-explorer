package datacache

import (
	"chainmaker_web/src/db"
	"chainmaker_web/src/sync/model"
	"testing"
)

func TestSetDelayedUpdateCache(t *testing.T) {
	//chainId := "chain1"
	blockHeight := int64(100)
	dealResult := model.ProcessedBlockData{
		Transactions: map[string]*db.Transaction{
			"tx1": {
				TxId: "tx1",
			},
		},
		GasRecordList: []*db.GasRecord{
			{
				TxId: "tx1",
			},
		},
		ContractEvents: []*db.ContractEvent{
			{
				ContractAddr: "contract1",
			},
		},
		UserList: map[string]*db.User{
			"user1": {
				UserAddr: "user1",
			},
		},
	}

	SetDelayedUpdateCache(ChainId, blockHeight, dealResult)

	cacheResult := GetDelayedUpdateCache(ChainId, blockHeight)
	if cacheResult == nil {
		t.Errorf("SetDelayedUpdateCache failed, cacheResult is nil")
	}
}

func TestSetCrossSubChainCrossCache(t *testing.T) {
	blockHeight := int64(100)
	dealResult := model.ProcessedBlockData{
		CrossChainResult: &model.CrossChainResult{
			CrossTransfer: map[string]*db.CrossTransactionTransfer{
				"CrossId": {
					CrossId: "CrossId",
				},
			},
		},
	}

	SetCrossSubChainCrossCache(ChainId, blockHeight, dealResult)

	crossTransfers, err := GetCrossTransfersCache(ChainId, blockHeight)
	if err != nil {
		t.Errorf("SetCrossSubChainCrossCache failed, err: %v", err)
	}
	if len(crossTransfers) == 0 {
		t.Errorf("SetCrossSubChainCrossCache failed, crossTransfers is empty")
	}
}

func TestGetRealtimeDataCache(t *testing.T) {
	height := int64(100)
	realtimeCacheData := model.NewGetRealtimeCacheData()

	GetRealtimeDataCache(ChainId, height, realtimeCacheData)
}

func TestGetRealtimeCrossCache(t *testing.T) {
	height := int64(100)
	realtimeCacheData := model.GetRealtimeCacheData{}

	if !GetRealtimeCrossCache(ChainId, height, &realtimeCacheData) {
		t.Errorf("GetRealtimeCrossCache failed")
	}
}

func TestGetCrossCycleTxDataCache(t *testing.T) {
	blockHeight := int64(100)

	crossCycleTxMap, err := GetCrossCycleTxDataCache(ChainId, blockHeight)
	if err != nil {
		t.Errorf("GetCrossCycleTxDataCache failed, err: %v", err)
	}
	if len(crossCycleTxMap) != 0 {
		t.Errorf("GetCrossCycleTxDataCache failed, crossCycleTxMap is not empty")
	}
}
