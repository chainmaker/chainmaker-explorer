/*
Package sync comment
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
SPDX-License-Identifier: Apache-2.0
*/
package datacache

import (
	"chainmaker_web/src/cache"
	"chainmaker_web/src/config"
	"chainmaker_web/src/db"
	"chainmaker_web/src/sync/model"
	"context"
	"encoding/json"
	"fmt"
	"strconv"
	"time"

	loggers "chainmaker_web/src/logger"

	"github.com/redis/go-redis/v9"
)

var (
	log = loggers.GetLogger(loggers.MODULE_SYNC)
)

// SetDelayedUpdateCache
//
//	@Description: 设置数据缓存，异步计算使用
//	@param chainId
//	@param blockHeight
//	@param dealResult
func SetDelayedUpdateCache(chainId string, blockHeight int64, dealResult model.ProcessedBlockData) {
	contractAddrs := make(map[string]string, 0)
	delayedUpdateData := model.NewGetRealtimeCacheData()
	if len(dealResult.Transactions) == 0 {
		return
	}

	for _, txInfo := range dealResult.Transactions {
		if txInfo.ContractAddr == "" {
			continue
		}
		contractAddrs[txInfo.ContractAddr] = txInfo.ContractAddr
	}
	for _, event := range dealResult.ContractEvents {
		if event.ContractAddr == "" {
			continue
		}
		contractAddrs[event.ContractAddr] = event.ContractAddr
	}
	delayedUpdateData.TxList = dealResult.Transactions
	delayedUpdateData.GasRecords = dealResult.GasRecordList
	delayedUpdateData.ContractEvents = dealResult.ContractEvents
	delayedUpdateData.UserInfoMap = dealResult.UserList
	delayedUpdateData.ContractAddrs = contractAddrs

	redisKey, expiration := cache.GetKeyDelayedUpdateData(chainId, blockHeight)
	retJson, _ := json.Marshal(delayedUpdateData)
	// 设置键值对和过期时间
	ctx := context.Background()
	_ = cache.GlobalRedisDb.Set(ctx, redisKey, string(retJson), expiration).Err()
}

// GetDelayedUpdateCache
//
//	@Description: 获取数据缓存，异步计算使用
//	@param chainId
//	@param blockHeight
//	@return *GetRealtimeCacheData 缓存数据
func GetDelayedUpdateCache(chainId string, blockHeight int64) *model.GetRealtimeCacheData {
	ctx := context.Background()
	redisKey, _ := cache.GetKeyDelayedUpdateData(chainId, blockHeight)
	redisRes := cache.GlobalRedisDb.Get(ctx, redisKey)
	if redisRes == nil || redisRes.Val() == "" {
		return nil
	}

	cacheResult := &model.GetRealtimeCacheData{}
	err := json.Unmarshal([]byte(redisRes.Val()), &cacheResult)
	if err != nil {
		return nil
	}
	return cacheResult
}

// SetCrossSubChainCrossCache
//
//	@Description: 缓存跨链信息，异步更新使用
//	@param chainId
//	@param blockHeight
//	@param dealResult
func SetCrossSubChainCrossCache(chainId string, blockHeight int64, dealResult model.ProcessedBlockData) {
	if dealResult.CrossChainResult == nil {
		return
	}
	crossTransferMap := dealResult.CrossChainResult.CrossTransfer
	if len(crossTransferMap) == 0 {
		return
	}

	crossTransfer := make([]*db.CrossTransactionTransfer, 0)
	for _, transfer := range crossTransferMap {
		crossTransfer = append(crossTransfer, transfer)
	}
	SetCrossTransfersCache(chainId, blockHeight, crossTransfer)
	//SetCrossCycleTxDataCache(chainId, blockHeight, saveCrossCycleTx)
}

// SetCrossTransfersCache
//
//	@Description: 缓存子链信息
//	@param chainId
//	@param blockHeight
//	@param crossTransfers 跨链流转数据
func SetCrossTransfersCache(chainId string, blockHeight int64, crossTransfers []*db.CrossTransactionTransfer) {
	if len(crossTransfers) == 0 {
		return
	}

	ctx := context.Background()
	prefix := config.GlobalConfig.RedisDB.Prefix
	heightStr := strconv.FormatInt(blockHeight, 10)
	redisKey := fmt.Sprintf(cache.RedisCrossTxTransfers, prefix, chainId, heightStr)
	retJson, err := json.Marshal(crossTransfers)
	if err != nil {
		log.Errorf("【Redis】set cache failed, key:%v, result:%v", redisKey, retJson)
		return
	}
	// 设置键值对和过期时间(1h 过期)
	_ = cache.GlobalRedisDb.Set(ctx, redisKey, string(retJson), time.Hour).Err()
}

// SetCrossCycleTxDataCache
//
//	@Description: 缓存子链跨链交易信息
//	@param chainId
//	@param blockHeight
//	@param crossCycleTxMap 跨链交易数据
func SetCrossCycleTxDataCache(chainId string, blockHeight int64, crossCycleTxMap map[string]*db.CrossCycleTransaction) {
	if len(crossCycleTxMap) == 0 {
		return
	}

	ctx := context.Background()
	prefix := config.GlobalConfig.RedisDB.Prefix
	heightStr := strconv.FormatInt(blockHeight, 10)
	redisKey := fmt.Sprintf(cache.RedisCrossCycleTxData, prefix, chainId, heightStr)
	retJson, err := json.Marshal(crossCycleTxMap)
	if err != nil {
		log.Errorf("【Redis】set cache failed, key:%v, result:%v", redisKey, retJson)
		return
	}
	// 设置键值对和过期时间(1h 过期)
	_ = cache.GlobalRedisDb.Set(ctx, redisKey, string(retJson), time.Hour).Err()
}

// GetCrossCycleTxDataCache
//
//	@Description: 获取子链信息缓存
//	@param chainId
//	@param blockHeight
//	@return map[string]*db.CrossCycleTransaction 跨链交易数据
//	@return error
func GetCrossCycleTxDataCache(chainId string, blockHeight int64) (map[string]*db.CrossCycleTransaction, error) {
	ctx := context.Background()
	cacheResult := make(map[string]*db.CrossCycleTransaction, 0)
	prefix := config.GlobalConfig.RedisDB.Prefix
	heightStr := strconv.FormatInt(blockHeight, 10)
	redisKey := fmt.Sprintf(cache.RedisCrossCycleTxData, prefix, chainId, heightStr)
	redisRes := cache.GlobalRedisDb.Get(ctx, redisKey)
	// 检查 Redis 错误
	if err := redisRes.Err(); err != nil {
		if err == redis.Nil {
			// 没有发生错误，缓存不存在
			return nil, nil
		}
		// 发生了错误
		log.Errorf("【Redis】get cache failed, key:%v, error:%v", redisKey, err)
		return nil, err
	}

	err := json.Unmarshal([]byte(redisRes.Val()), &cacheResult)
	if err != nil {
		log.Errorf("【Redis】get cache failed, key:%v, result:%v", redisKey, redisRes)
		return nil, err
	}
	return cacheResult, nil
}

// GetRealtimeDataCache 获取实时数据缓存
// @param chainId 链ID
// @param height 区块高度
// @param realtimeCacheData 实时数据缓存
// @return bool 是否获取成功
func GetRealtimeDataCache(chainId string, height int64, realtimeCacheData *model.GetRealtimeCacheData) bool {
	// 获取缓存数据
	cacheResult := GetDelayedUpdateCache(chainId, height)
	if cacheResult == nil {
		return false
	}

	// 合并缓存数据到 delayedUpdateData
	for k, v := range cacheResult.TxList {
		realtimeCacheData.TxList[k] = v
	}
	// 合并缓存数据到 delayedUpdateData
	for userAddr, user := range cacheResult.UserInfoMap {
		realtimeCacheData.UserInfoMap[userAddr] = user
	}

	for _, addr := range cacheResult.ContractAddrs {
		realtimeCacheData.ContractAddrs[addr] = addr
	}

	realtimeCacheData.GasRecords = append(realtimeCacheData.GasRecords, cacheResult.GasRecords...)
	realtimeCacheData.ContractEvents = append(realtimeCacheData.ContractEvents, cacheResult.ContractEvents...)
	return true
}

// GetCrossTransfersCache
//
//	@Description: 获取子链信息缓存
//	@param chainId
//	@param blockHeight
//	@return []*db.CrossTransactionTransfer 跨链流转信息
//	@return error
func GetCrossTransfersCache(chainId string, blockHeight int64) ([]*db.CrossTransactionTransfer, error) {
	ctx := context.Background()
	var cacheResult []*db.CrossTransactionTransfer
	prefix := config.GlobalConfig.RedisDB.Prefix
	heightStr := strconv.FormatInt(blockHeight, 10)
	redisKey := fmt.Sprintf(cache.RedisCrossTxTransfers, prefix, chainId, heightStr)
	redisRes := cache.GlobalRedisDb.Get(ctx, redisKey)
	// 检查 Redis 错误
	if err := redisRes.Err(); err != nil {
		if err == redis.Nil {
			// 没有发生错误，缓存不存在
			return nil, nil
		}
		// 发生了错误
		log.Errorf("【Redis】get cache failed, key:%v, error:%v", redisKey, err)
		return nil, err
	}

	err := json.Unmarshal([]byte(redisRes.Val()), &cacheResult)
	if err != nil {
		log.Errorf("【Redis】get cache failed, key:%v, result:%v", redisKey, redisRes)
		return nil, err
	}
	return cacheResult, nil
}

// GetRealtimeCrossCache 获取跨链信息缓存
// @param chainId 子链id
// @param height 区块高度
// @param realtimeCacheData 实时缓存数据
// @return bool 是否成功
func GetRealtimeCrossCache(chainId string, height int64, realtimeCacheData *model.GetRealtimeCacheData) bool {
	//获取主子链缓存
	crossCycleTransfers, err := GetCrossTransfersCache(chainId, height)
	if err != nil {
		return false
	}

	realtimeCacheData.CrossTransfers = append(realtimeCacheData.CrossTransfers, crossCycleTransfers...)
	return true
}
