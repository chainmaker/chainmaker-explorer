#!/usr/bin/env bash
#
# Copyright (C) BABEC. All rights reserved.
# Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
#
# SPDX-License-Identifier: Apache-2.0
#

function start_mysql_container() {
  # 检查 MySQL 容器是否已经存在
  if docker ps -a | grep -q ut-mysql-test; then 
    echo "MySQL Docker 容器已存在，正在停止并删除..." 
    docker stop ut-mysql-test
    docker rm ut-mysql-test
  fi


  # 启动 MySQL Docker 容器
  docker run --name ut-mysql-test -e MYSQL_ROOT_PASSWORD=123456 -e MYSQL_DATABASE=chainmaker_explorer_dev -d -p 33061:3306 mysql:8.0

  # 等待 MySQL 启动
  sleep 20

  # 设置 WEAVIATE_URL 环境变量
  DB_URL="root:123456@tcp(127.0.0.1:33061)/chainmaker_explorer_dev"
  export WEAVIATE_URL=$DB_URL

  echo "MySQL Docker 容器已启动，WEAVIATE_URL 设置为: $WEAVIATE_URL"
}

function ut_cover() {
  # 启动 MySQL Docker 容器
  start_mysql_container

  cd ${cm}/$1
  echo "cd ${cm}/$1"
  echo "exec go test"
  go test -coverprofile cover.out ./...
  total=$(go tool cover -func=cover.out | tail -1)
  echo ${total}
  rm cover.out
  coverage=$(echo ${total} | grep -P '\d+\.\d+(?=\%)' -o) #如果macOS 不支持grep -P选项，可以通过brew install grep更新grep
  #计算注释覆盖率，需要安装gocloc： go install github.com/hhatto/gocloc/cmd/gocloc@latest
  comment_coverage=$(gocloc --include-lang=Go --output-type=json --not-match=".*_test\.go" . | jq '(.total.comment-.total.files*6)/(.total.code+.total.comment)*100')
  echo "注释率：${comment_coverage}%"

  # 如果测试覆盖率低于N，认为ut执行失败
  (( $(awk "BEGIN {print (${coverage} >= $2)}") )) || (echo "$1 单测覆盖率: ${coverage} 低于 $2%"; exit 1)
  (( $(awk "BEGIN {print (${comment_coverage} >= $3)}") )) || (echo "$1 注释覆盖率: ${comment_coverage} 低于 $3%"; exit 1)

  # 清理 Docker 容器
  docker stop ut-mysql-test
  docker rm ut-mysql-test
}
set -e

cm=$(pwd)

if [[ $cm == *"scripts" ]] ;then
  cm=$cm/..
fi

ut_cover src 50 15

